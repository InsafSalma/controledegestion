import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.border.LineBorder;

import classes.Data;
import classes.Util;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class IndicateurRentabilite extends JPanel {
	static JTextField textField;
	static JTextField textField_1;
	static JTextField textField_2;
	static JTextField textField_3;
	static JTextField textField_4;
	static double margeSec,indiceSec,lO;

	/**
	 * Create the panel.
	 */
	public IndicateurRentabilite() {
		setBackground(new Color(255, 255, 255));
		//setBackground(new Color(255, 255, 255));
		setBounds(0, 45, 1042, 534);
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBackground(new Color(248, 248, 255));
		panel.setBounds(39, 57, 950, 466);
		add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Marge sur co\u00FBt variable");
		lblNewLabel.setForeground(new Color(0, 0, 128));
		lblNewLabel.setBounds(160, 11, 296, 48);
		panel.add(lblNewLabel);
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 18));
		
		JLabel lblMargeSurCot = new JLabel("Marge sur co\u00FBt sp\u00E9cifique");
		lblMargeSurCot.setForeground(new Color(0, 0, 128));
		lblMargeSurCot.setBounds(160, 85, 296, 48);
		panel.add(lblMargeSurCot);
		lblMargeSurCot.setFont(new Font("Times New Roman", Font.BOLD, 18));
		
		JLabel lblRsultat = new JLabel("R\u00E9sultat");
		lblRsultat.setForeground(new Color(0, 0, 128));
		lblRsultat.setBounds(160, 166, 296, 48);
		panel.add(lblRsultat);
		lblRsultat.setFont(new Font("Times New Roman", Font.BOLD, 18));
		
		JLabel lblSeuilDeRentabilit = new JLabel("Seuil de rentabilit\u00E9");
		lblSeuilDeRentabilit.setForeground(new Color(0, 0, 128));
		lblSeuilDeRentabilit.setBounds(160, 244, 296, 48);
		panel.add(lblSeuilDeRentabilit);
		lblSeuilDeRentabilit.setFont(new Font("Times New Roman", Font.BOLD, 18));
		
		JLabel lblTauxDeMarge = new JLabel("Taux de marge sur co\u00FBt variable");
		lblTauxDeMarge.setForeground(new Color(0, 0, 128));
		lblTauxDeMarge.setBounds(160, 307, 296, 48);
		panel.add(lblTauxDeMarge);
		lblTauxDeMarge.setFont(new Font("Times New Roman", Font.BOLD, 18));
		
		textField = new JTextField();
		textField.setBounds(466, 11, 291, 48);
		panel.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(466, 85, 291, 48);
		panel.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(466, 166, 291, 48);
		panel.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(466, 244, 291, 48);
		panel.add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(466, 307, 291, 48);
		panel.add(textField_4);
		
		JButton btnNewButton = new JButton("Valider");
		btnNewButton.setBackground(new Color(0, 0, 128));
		btnNewButton.setForeground(new Color(255, 255, 255));
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Data data=new Data();
				margeSec=data.margeDeSecurite(Donnees.ca, Donnees.seuilDeRentabilite);
				indiceSec=data.indiceDeSecurite(margeSec, Donnees.ca);
				lO=data.levierOp(Donnees.mcv, Donnees.resultat);
				
				IndicateurSecurite.textField.setText(String.valueOf(indiceSec));
				IndicateurSecurite.textField_1.setText(String.valueOf(margeSec));
				IndicateurSecurite.textField_2.setText(String.valueOf(lO));

				textField.setText("");
				textField_1.setText("");
				textField_2.setText("");
				textField_3.setText("");
				textField_4.setText("");
				
				CoutSpecifique.panelDonnees.setVisible(false);
				CoutSpecifique.panelIndicateurSec.setVisible(true);
				CoutSpecifique.panelIndicateurRentab.setVisible(false);
				
				Util.currentPanel(CoutSpecifique.btnIndicateursDeScurit);
				Util.normalFormat(CoutSpecifique.btnDonnes);
				Util.normalFormat(CoutSpecifique.btnIndicateursDeRentabilits);
				Util.normalFormat(CoutSpecifique.btnNewButton);
			}
		});
		btnNewButton.setBounds(346, 394, 252, 48);
		panel.add(btnNewButton);
	}
}
