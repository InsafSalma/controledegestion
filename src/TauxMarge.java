import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import models.ModelTauxM;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TauxMarge extends JPanel {
	private JTextField textField;
	private JTable table;
	private ModelTauxM modelTauxM;

	/**
	 * Create the panel.
	 */
	public TauxMarge() {
		setBackground(Color.WHITE);
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 128)));
		panel.setBounds(74, 11, 851, 422);
		panel.setBackground(new Color(248, 248, 255));
		add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Total : ");
		lblNewLabel.setForeground(new Color(0, 0, 128));
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 18));
		lblNewLabel.setBounds(231, 286, 102, 37);
		panel.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(370, 286, 221, 38);
		panel.add(textField);
		
		JButton btnNewButton = new JButton("Calculer");
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnNewButton.setBackground(new Color(0, 0, 128));
		btnNewButton.setBounds(187, 347, 208, 44);
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Retour");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				DirectCosting.directCostingAcceuil.setVisible(true);
				DirectCosting.btnTitre.setText("");
				DirectCosting.btnTitre.setVisible(false);
			}
		});
		btnNewButton_1.setForeground(Color.WHITE);
		btnNewButton_1.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnNewButton_1.setBackground(new Color(0, 0, 128));
		btnNewButton_1.setBounds(417, 347, 208, 44);
		panel.add(btnNewButton_1);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(50, 53, 761, 208);
		panel.add(scrollPane);
		
		modelTauxM=new ModelTauxM();
		
		table = new JTable(modelTauxM);
		scrollPane.setViewportView(table);

	}
}
