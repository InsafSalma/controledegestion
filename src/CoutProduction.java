import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import classes.CentreAnalyse;
import classes.Charge;
import classes.MatierePremiere;
import classes.Produit;
import classes.Util;
import models.ModeleProduction;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CoutProduction extends JPanel {
	private JTextField textFieldQte;
	private JTextField textFieldCUO;
	private JTextField textFieldMontant;
	private JTable table;
	private JComboBox<String> comboBox;
	private JComboBox<String> comboBoxProduits;
	private JComboBox<String> comboBoxCAP;
	private JScrollPane scrollPane;
	private JTextField textField;
	private ModeleProduction modele;
	private MatierePremiere matiere;
	private Produit produit = null;
	private ArrayList<Charge> charges;
	private Charge charge;
	private CentreAnalyse centreAnalyse;
	private boolean stock = false;
	private boolean fifo = false;
	private JTextField textFieldCUMP;
	public static boolean noStock = false;
	// stock est vrai si ce Jpanel est ouvert apr�s la valorisation des stocks

	/**
	 * Create the panel.
	 */
	public CoutProduction() {
		setBackground(new Color(248, 248, 255));
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setBounds(0, 45, 1042, 534);
		setLayout(null);

		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 128)));
		panel.setBounds(10, 58, 384, 393);
		panel.setBackground(new Color(255, 250, 250));
		add(panel);
		panel.setLayout(null);

		JLabel lblNewLabel = new JLabel("Les charges ");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lblNewLabel.setForeground(new Color(0, 0, 128));
		lblNewLabel.setBounds(10, 63, 154, 41);
		panel.add(lblNewLabel);

		JLabel lblQuantit = new JLabel("Quantit\u00E9");
		lblQuantit.setForeground(new Color(0, 0, 128));
		lblQuantit.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lblQuantit.setBounds(10, 115, 154, 41);
		panel.add(lblQuantit);

		JLabel lblCotDunitDuvre = new JLabel("Co\u00FBt d'unit\u00E9 d'\u0153uvre");
		lblCotDunitDuvre.setForeground(new Color(0, 0, 128));
		lblCotDunitDuvre.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lblCotDunitDuvre.setBounds(10, 219, 154, 41);
		panel.add(lblCotDunitDuvre);

		JLabel lblMontant = new JLabel("Montant");
		lblMontant.setForeground(new Color(0, 0, 128));
		lblMontant.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lblMontant.setBounds(10, 271, 154, 41);
		panel.add(lblMontant);

		textFieldQte = new JTextField();
		textFieldQte.setBounds(174, 115, 200, 41);
		panel.add(textFieldQte);
		textFieldQte.setColumns(10);
		textFieldQte.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (Integer.parseInt(textFieldQte.getText().toString()) >= 0) {
					calculMontant();
				}
			}
		});

		textFieldCUO = new JTextField();
		textFieldCUO.setColumns(10);
		textFieldCUO.setBounds(174, 219, 200, 41);
		panel.add(textFieldCUO);

		textFieldMontant = new JTextField();
		textFieldMontant.setColumns(10);
		textFieldMontant.setBounds(174, 271, 200, 41);
		panel.add(textFieldMontant);

		JButton btnAjouter = new JButton("Ajouter");
		btnAjouter.setForeground(new Color(255, 255, 255));
		btnAjouter.setBackground(new Color(0, 0, 128));
		btnAjouter.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnAjouter.setBounds(10, 323, 364, 41);
		panel.add(btnAjouter);
		btnAjouter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (Double.parseDouble(textFieldMontant.getText().toString()) >= 0) {
						if (charge != null) {
							charge.setMontant(Double.parseDouble(textFieldMontant.getText().toString()));
						} else {
							matiere.setMontantAppro(Double.parseDouble(textFieldMontant.getText().toString()));
						}
					}
				} catch (NumberFormatException ex) {
					JOptionPane.showMessageDialog(null, " Veuillez appuyer sur entrer apr�s la saisie de la quantit�! ",
							"Attention", 1);
				}
				if (charge != null) {
					for (Charge ch : produit.getCharges()) {
						if (charge.getNom() == ch.getNom()) {
							ch.setMontant(charge.getMontant());
							ch.setQuantite(charge.getQuantite());
							break;
						}
					}
				} else {
					if (matiere != null)
						produit.getComposants().add(matiere);
				}
				produit.calculCoutProduction();
				for (Produit p : Util.produits) {
					if (p.getNom() == produit.getNom()) {
						p = produit.copy();
						break;
					}
				}
				modele.updateProduits(Util.produits);
			}
		});

		comboBox = new JComboBox<>();
		comboBox.setBounds(174, 68, 200, 33);
		panel.add(comboBox);
		comboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				selectedItem(e);
			}
		});

		JLabel lblProduit = new JLabel("Produit:");
		lblProduit.setBounds(10, 11, 154, 41);
		panel.add(lblProduit);
		lblProduit.setForeground(new Color(0, 0, 128));
		lblProduit.setFont(new Font("Times New Roman", Font.BOLD, 15));

		comboBoxProduits = new JComboBox<>();
		comboBoxProduits.setBounds(174, 16, 200, 33);
		panel.add(comboBoxProduits);
		comboBoxProduits.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				selectedProduit(e);
			}
		});

		
		JLabel lblCUMP = new JLabel("CUMP");
		lblCUMP.setForeground(new Color(0, 0, 128));
		lblCUMP.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lblCUMP.setBounds(10, 167, 154, 41);
		panel.add(lblCUMP);

		textFieldCUMP = new JTextField();
		textFieldCUMP.setColumns(10);
		textFieldCUMP.setBounds(174, 167, 200, 41);
		panel.add(textFieldCUMP);

		scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(new LineBorder(new Color(0, 0, 128), 1, true));
		scrollPane.setBounds(418, 156, 602, 295);
		add(scrollPane);

		JButton btnValider = new JButton("Valider");
		btnValider.setBackground(new Color(0, 0, 128));
		btnValider.setForeground(new Color(255, 255, 255));
		btnValider.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnValider.setBounds(344, 481, 290, 42);
		add(btnValider);
		btnValider.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				 String[] dialogButton = new String[] {"Oui", "Non"};
				 int dialogResult = JOptionPane.showOptionDialog(null, "Avez-vous du stock ?", "Stock",
				        JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
				        null, dialogButton, dialogButton[0]);
				 
				if(dialogResult == 0) {

					String[] options = new String[] { "CUMP", "FIFO" };
					int response = JOptionPane.showOptionDialog(null,
							"Vous voulez utiliser quelle m�thode de valorisation des stocks?", "Choix de m�thode",
							JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[0]);

					if (response == 0) {
						setVisible(false);
						CoutComplet.panelCUMP.setVisible(true);
						CoutComplet.panelCUMP.isAfterProduit();
						CUMPStock.lblNewLabel.setText("");
						CUMPStock.lblNewLabel.setText("Produits");
						CoutComplet.panelCUMP.fillComboBoxAndTable();
					} else {
						setVisible(false);
						CoutComplet.panelFIFOStock.setVisible(true);
						CoutComplet.panelFIFOStock.isAfterProduit();
						FIFOStock.lblNewLabel.setText("");
						FIFOStock.lblNewLabel.setText("Produits");
						CoutComplet.panelFIFOStock.fillComboBoxAndTable();
					}

				} else {
					setVisible(false);
					CoutComplet.panelCoutRevient.setVisible(true);
					CoutComplet.panelCoutRevient.initialiserModel();
					Util.currentPanel(CoutComplet.btnCoutRevient);
					Util.normalFormat(CoutComplet.btnCotDapprovisionnement);
					Util.normalFormat(CoutComplet.btnCotDeProduction);
					Util.normalFormat(CoutComplet.btnCotDunitDuvre);
					Util.normalFormat(CoutComplet.btnCentresDanalyses);
				}
			}
		});

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 128)));
		panel_1.setBounds(419, 58, 602, 94);
		panel_1.setBackground(new Color(255, 250, 250));
		add(panel_1);
		panel_1.setLayout(null);

		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(153, 7, 200, 32);
		panel_1.add(textField);

		JLabel lbCharges = new JLabel("Charge:");
		lbCharges.setForeground(new Color(0, 0, 128));
		lbCharges.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lbCharges.setBounds(10, 2, 154, 41);
		panel_1.add(lbCharges);

		charges = new ArrayList<>();

		JButton btnNvCharge = new JButton("Ajouter");
		btnNvCharge.setBackground(new Color(0, 0, 128));
		btnNvCharge.setForeground(new Color(255, 255, 255));
		btnNvCharge.setFont(new Font("Times New Roman", Font.BOLD, 16));
		btnNvCharge.setBounds(405, 23, 173, 48);
		panel_1.add(btnNvCharge);
		btnNvCharge.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Charge charge = new Charge(textField.getText().toString(), centreAnalyse);
				charges.add(charge);
				modele.refill(charge);
				textField.setText("");
				fillComboBox();
			}
		});

		JLabel lblCentreDanalyse = new JLabel("Centre d'analyse:");
		lblCentreDanalyse.setForeground(new Color(0, 0, 128));
		lblCentreDanalyse.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lblCentreDanalyse.setBounds(10, 45, 154, 41);
		panel_1.add(lblCentreDanalyse);

		comboBoxCAP = new JComboBox<>();
		comboBoxCAP.setBounds(153, 50, 200, 33);
		panel_1.add(comboBoxCAP);
		
		JButton btnNewButton_1 = new JButton("?");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "- Commencez par saisir les nom des charges puis choisissez un centre d'analyse ou aucun et appuyer sur le bouton ajouter. "
						+ "\n- Choisissez le produit ensuite un � un les charges"
						+ "\nPour chaque charge vous pouvez:"
						+ "\n       - saisir la quantit� et appuyer sur entrer afin que le montant s'affiche."
						+ "\n       - saisir le montant directement."
						+ "\n- Appuyez sur le bouton ajouter pour chaque charge."
						+ "\n- Appuyez sur le bouton valider pour passer � l'�tape suivante.", "Informations", 1);
			}
		});
		btnNewButton_1.setForeground(Color.WHITE);
		btnNewButton_1.setFont(new Font("Times New Roman", Font.BOLD, 15));
		btnNewButton_1.setBackground(new Color(0, 0, 128));
		btnNewButton_1.setBounds(10, 11, 59, 36);
		add(btnNewButton_1);
		comboBoxCAP.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				selectedCentreAnalyse(e);
			}
		});

	}

	public void fillComboBox() {
		comboBox.removeAllItems();
		comboBoxProduits.removeAllItems();
		comboBoxCAP.removeAllItems();
		for (MatierePremiere mp : Util.matierePremieres) {
			comboBox.addItem(mp.getNom());
		}
		if (charges != null) {
			for (Charge charge : charges) {
				comboBox.addItem(charge.getNom());
			}
		}
		for (Produit p : Util.produits) {
			comboBoxProduits.addItem(p.getNom());
		}
		for (CentreAnalyse ca : Util.centreAnalysePrincipaux) {
			comboBoxCAP.addItem(ca.getNom());
		}
		comboBoxCAP.addItem("Aucun");
	}

	public void fillTable() {
		modele = new ModeleProduction(Util.produits, Util.matierePremieres);
		table = new JTable(modele);
		CoutComplet.tableStyle(table);
		scrollPane.setViewportView(table);
	}

	public void selectedItem(ActionEvent e) {
		charge = null;
		matiere = null;
		this.textFieldCUO.setEnabled(true);
		this.textFieldQte.setText("");
		this.textFieldMontant.setText("");
		this.textFieldCUMP.setText("");
		if (comboBox.getSelectedItem() != null) {
			for (MatierePremiere mp : Util.matierePremieres) {
				if (mp.getNom() == comboBox.getSelectedItem().toString()) {
					matiere = mp.copy();
					this.textFieldCUO.setEnabled(false);
					if(noStock)
					{
						this.textFieldCUMP.setEnabled(false);
					}
					else
					{
						this.textFieldCUMP.setEnabled(true);
						if (!this.fifo) {
							this.textFieldCUMP.setText(String.valueOf(this.matiere.getStock().getCump()));
						} else if (this.fifo) {
							this.textFieldCUMP.setText(String.valueOf(this.matiere.getStockFIFO().getCUMP()));
						}
					}
					break;
				}
			}
			for (Charge charge : this.charges) {
				if (charge.getNom() == comboBox.getSelectedItem().toString()) {
					this.charge = charge.copy();
					this.textFieldCUMP.setEnabled(false);
					if(this.charge.getCentreAnalyse() != null)
					{
						this.textFieldCUO.setText(String.valueOf(this.charge.getCentreAnalyse().calculCoutUO()));
					}else {
						this.textFieldCUO.setEnabled(false);
					}
					break;
				}
			}
		}
	}

	public void selectedProduit(ActionEvent e) {
		this.textFieldCUO.setText("");
		this.textFieldQte.setText("");
		this.textFieldMontant.setText("");
		this.textFieldCUMP.setText("");
		produit = null;
		if (comboBoxProduits.getSelectedItem() != null) {
			for (Produit p : Util.produits) {
				if (p.getNom() == comboBoxProduits.getSelectedItem().toString()) {
					produit = p;
				}
			}
		}
	}

	public void selectedCentreAnalyse(ActionEvent e) {
		if (comboBoxCAP.getSelectedItem() != null) {
			for (CentreAnalyse ca : Util.centreAnalysePrincipaux) {
				if (ca.getNom() == comboBoxCAP.getSelectedItem().toString()) {
					centreAnalyse = ca.copy();
				}else if( comboBoxCAP.getSelectedItem().toString() == "Aucun")
				{
					centreAnalyse = null;
				}
			}
		}
	}

	public void calculMontant() {
		if (charge != null) {
			charge.setQuantite(Integer.parseInt(textFieldQte.getText().toString()));
			charge.calculeMontant();
			textFieldMontant.setText(String.valueOf(charge.getMontant()));
		} else {
			if (!stock) {
				matiere.calculMontantAppro(Integer.parseInt(textFieldQte.getText().toString()));
			} else {
				matiere.calculMontantAfterStock(Integer.parseInt(textFieldQte.getText().toString()));
			}
			textFieldMontant.setText(String.valueOf(matiere.getMontantAppro()));
		}
	}

	public void setFifo() {
		this.fifo = true;
	}

	public void isAfterStock(boolean stock) {
		this.stock = stock;
		modele.isAfterStock(stock);
	}
}
