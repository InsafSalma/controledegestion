import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import classes.CentreAnalyse;
import classes.MatierePremiere;
import classes.Produit;
import classes.StockCUMP;
import classes.Util;
import models.ModelCUMP;
import models.ModelProduitsCUMP;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;

public class CUMPStock extends JPanel {
	private JTable table;
	private JTable table_1;
	private JComboBox<String> comboBox;
	private JTable table_2;
	private ModelProduitsCUMP modelresultats;
	private JScrollPane scrollPane_1, scrollPane, scrollPane_2;
	private MatierePremiere matiere;
	private Produit produit;
	private StockCUMP stock = new StockCUMP();
	private ModelCUMP modelEntrees, modelSorties;
	private ArrayList<String> resultats;
	private boolean afterProduit = false;
	public static JLabel lblNewLabel = null;
	private JButton btnNewButton;

	/**
	 * Create the panel.
	 */
	public CUMPStock() {
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setBounds(0, 45, 1042, 534);
		setBackground(new Color(255, 255, 255));
		setLayout(null);

		lblNewLabel = new JLabel();
		lblNewLabel.setText("Mati\u00E8re premi\u00E8re:");
		lblNewLabel.setForeground(new Color(0, 0, 128));
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 17));
		lblNewLabel.setBounds(341, 55, 156, 19);
		add(lblNewLabel);

		comboBox = new JComboBox<>();
		comboBox.setBounds(527, 47, 193, 33);
		add(comboBox);
		comboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				selectedItem(e);
			}
		});

		scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(new LineBorder(new Color(0, 0, 128)));
		scrollPane.setBounds(134, 102, 353, 108);
		add(scrollPane);

		scrollPane_1 = new JScrollPane();
		scrollPane_1.setViewportBorder(new LineBorder(new Color(0, 0, 128)));
		scrollPane_1.setBounds(542, 102, 353, 108);
		add(scrollPane_1);

		JButton btnAjouter = new JButton("Ajouter");
		btnAjouter.setForeground(new Color(255, 255, 255));
		btnAjouter.setBackground(new Color(0, 0, 128));
		btnAjouter.setFont(new Font("Times New Roman", Font.BOLD, 16));
		btnAjouter.setBounds(411, 231, 211, 33);
		add(btnAjouter);
		btnAjouter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!afterProduit) {
					matiere.setStock(stock);
					for (MatierePremiere mp : Util.matierePremieres) {
						if (mp.getNom() == matiere.getNom()) {
							mp.setStock(stock);
							break;
						}
					}
					modelresultats.updateMatieres(Util.matierePremieres);
				} else {
					produit.setStockCump(stock);
					for (Produit p : Util.produits) {
						if (p.getNom() == produit.getNom()) {
							p.setStockCump(stock);
							break;
						}
					}
					modelresultats.updateProduits(Util.produits);
				}
				stock = new StockCUMP();
				modelEntrees.setStock(stock);
				modelSorties.setStock(stock);
			}
		});

		JButton btnValider = new JButton("Valider");
		btnValider.setBackground(new Color(0, 0, 128));
		btnValider.setForeground(new Color(255, 255, 255));
		btnValider.setFont(new Font("Times New Roman", Font.BOLD, 16));
		btnValider.setBounds(411, 472, 211, 33);
		add(btnValider);
		btnValider.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				if (!afterProduit) {
					CoutComplet.panelProNonStock.setVisible(true);
					CoutComplet.panelProNonStock.fillTable();
					CoutComplet.panelProNonStock.fillComboBox();
					CoutComplet.panelProNonStock.isAfterStock(true);
					Util.currentPanel(CoutComplet.btnCotDeProduction);
					Util.normalFormat(CoutComplet.btnCoutRevient);
				} else {
					for (Produit p : Util.produits) {
						p.updateCoutProduction();
					}
					CoutComplet.panelCoutRevient.setVisible(true);
					CoutComplet.panelCoutRevient.initialiserModel();
					Util.currentPanel(CoutComplet.btnCoutRevient);
					Util.normalFormat(CoutComplet.btnCotDeProduction);
				}
				Util.normalFormat(CoutComplet.btnCotDapprovisionnement);
				Util.normalFormat(CoutComplet.btnCotDunitDuvre);
				Util.normalFormat(CoutComplet.btnCentresDanalyses);
			}
		});

		scrollPane_2 = new JScrollPane();
		scrollPane_2.setViewportBorder(new LineBorder(new Color(0, 0, 128)));
		scrollPane_2.setBounds(134, 276, 761, 181);
		add(scrollPane_2);

		btnNewButton = new JButton("?");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!afterProduit)
				{
					JOptionPane.showMessageDialog(null, "- Choisissez la mati�re premi�re de la liste d�roulante"
							+ "\n- Saisissez la quantit� initial et son montant."
							+ "\n- Saisissez la quantit� final et la quantit� des sorties."
							+ "\n- Appuyer sur le bouton Ajouter."
							+ "\n  !!!Appuyer sur entrer apr�s chaque saisie au tableau!!!"
							+ "\n- Appuyez sur le bouton valider pour passer � l'�tape suivante.", "Informations", 1);	
				}else {
					JOptionPane.showMessageDialog(null, "- Choisissez le produit de la liste d�roulante"
							+ "\n- Saisissez la quantit� initial et son montant."
							+ "\n- Saisissez la quantit� des entr�es"
							+ "\n- Saisissez la quantit� final et la quantit� des sorties."
							+ "\n- Appuyer sur le bouton Ajouter."
							+ "\n  !!!Appuyer sur entrer apr�s chaque saisie au tableau!!!"
							+ "\n- Appuyez sur le bouton valider pour passer � l'�tape suivante.", "Informations", 1);
				}
			}
		});
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 15));
		btnNewButton.setBackground(new Color(0, 0, 128));
		btnNewButton.setBounds(22, 29, 59, 36);
		add(btnNewButton);
	}

	public void fillComboBoxAndTable() {
		comboBox.removeAllItems();

		ArrayList<String> entrees = new ArrayList<>();
		entrees.add("Stock initial");
		entrees.add("Entr�es");
		entrees.add("TOTAL");
		modelEntrees = new ModelCUMP(entrees, 0, this.stock, this.afterProduit);

		table = new JTable(modelEntrees);
		CoutComplet.tableStyle(table);
		scrollPane.setViewportView(table);

		ArrayList<String> sorties = new ArrayList<>();
		sorties.add("Stock final");
		sorties.add("Sorties");
		sorties.add("TOTAL");
		modelSorties = new ModelCUMP(sorties, 1, this.stock, this.afterProduit);

		table_1 = new JTable(modelSorties);
		CoutComplet.tableStyle(table_1);
		scrollPane_1.setViewportView(table_1);

		resultats = new ArrayList<>();
		resultats.add("Montant Stock initial");
		resultats.add("Montant entr�es");
		resultats.add("Montant sorties");
		resultats.add("Montant Stock final");
		if (!afterProduit) {
			for (MatierePremiere mp : Util.matierePremieres) {
				comboBox.addItem(mp.getNom());
			}
			this.modelresultats = new ModelProduitsCUMP(resultats, Util.matierePremieres);
		} else {
			for (Produit p : Util.produits) {
				comboBox.addItem(p.getNom());
			}
			this.modelresultats = new ModelProduitsCUMP(resultats, Util.produits, this.afterProduit);
		}
		table_2 = new JTable(this.modelresultats);
		CoutComplet.tableStyle(table_2);
		scrollPane_2.setViewportView(table_2);
	}

	public void selectedItem(ActionEvent e) {
		if (comboBox.getSelectedItem() != null) {
			if (!afterProduit) {
				for (MatierePremiere mp : Util.matierePremieres) {
					if (mp.getNom() == comboBox.getSelectedItem().toString()) {
						matiere = mp.copy();
						stock.setMontantEntrees(matiere.getCout());
						stock.setQuantiteEntrees(matiere.getQuantiteAchete());
						modelEntrees.setStock(stock);
					}
				}
			} else {
				for (Produit p : Util.produits) {
					if (p.getNom() == comboBox.getSelectedItem().toString()) {
						produit = p.copy();
						stock.setMontantEntrees(produit.getCout().getCoutProduction());
						modelEntrees.setStock(stock);
					}
				}
			}

		}
	}

	public void isAfterProduit() {
		this.afterProduit = true;
	}
}
