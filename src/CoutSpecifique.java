import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import classes.Util;

import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JToggleButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CoutSpecifique extends JFrame {

	private JPanel contentPane;
	public static Donnees panelDonnees;
	public static IndicateurRentabilite panelIndicateurRentab;
	public static IndicateurSecurite panelIndicateurSec;
	public static CoefficientElasticite panelCoeffElasticite;
	public static JButton btnNewButton,btnIndicateursDeScurit,btnIndicateursDeRentabilits,btnDonnes;

	public CoutSpecifique() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1056, 616);
		setTitle("Co�t sp�cifique");
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		panelDonnees=new Donnees();
		contentPane.add(panelDonnees);
		
		panelIndicateurRentab=new IndicateurRentabilite();
		contentPane.add(panelIndicateurRentab);
		panelIndicateurRentab.setVisible(false);
		
		panelIndicateurSec=new IndicateurSecurite();
		contentPane.add(panelIndicateurSec);
		panelIndicateurSec.setVisible(false);
		
		panelCoeffElasticite=new CoefficientElasticite();
		contentPane.add(panelCoeffElasticite);
		panelCoeffElasticite.setVisible(false);
		
		btnDonnes = new JButton("Donn\u00E9es");
		btnIndicateursDeRentabilits = new JButton("Indicateurs de rentabilit\u00E9s");
		btnIndicateursDeScurit = new JButton("Indicateurs de s\u00E9curit\u00E9");
		btnNewButton = new JButton("Coefficients d\u2019\u00E9lasticit\u00E9");
		
		
		
		Util.normalFormat(btnNewButton);
	
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 16));
		btnNewButton.setBounds(838, 0, 204, 46);
		contentPane.add(btnNewButton);
		
		Util.normalFormat(btnIndicateursDeScurit);
		
		btnIndicateursDeScurit.setFont(new Font("Times New Roman", Font.BOLD, 16));
		btnIndicateursDeScurit.setBounds(639, 0, 210, 46);
		contentPane.add(btnIndicateursDeScurit);
		
		Util.normalFormat(btnIndicateursDeRentabilits);
		
		btnIndicateursDeRentabilits.setFont(new Font("Times New Roman", Font.BOLD, 16));
		btnIndicateursDeRentabilits.setBounds(422, -1, 223, 46);
		contentPane.add(btnIndicateursDeRentabilits);
		
		
		Util.normalFormat(btnDonnes);
		btnDonnes.setFont(new Font("Times New Roman", Font.BOLD, 16));
		btnDonnes.setBounds(286, -1, 146, 46);
		contentPane.add(btnDonnes);
		Util.currentPanel(btnDonnes);
		
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent e) {
                try {
                    Authentification.menuFrame.setVisible(true);
                    e.getWindow().dispose();
                } catch (java.lang.NullPointerException ex) {
                    e.getWindow().dispose();
                }
            }
        });
	}
	
	
}
