import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import classes.Data;
import classes.Util;

import java.awt.Color;

public class IndicateurSecurite extends JPanel {
	static JTextField textField;
	static JTextField textField_1;
	static JTextField textField_2;
	static JTextField textField_3;
	private int index;
	static double pointM;
	static String pointMC;

	/**
	 * Create the panel.
	 */
	public IndicateurSecurite() {
		setBackground(new Color(255, 255, 255));
		setBounds(0, 45, 1042, 534);
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBackground(new Color(248, 248, 255));
		panel.setBounds(10, 48, 386, 359);
		add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Comment voulez-vous calculer votre point mort ?");
		lblNewLabel.setForeground(new Color(0, 0, 128));
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 17));
		lblNewLabel.setBounds(10, 35, 376, 68);
		panel.add(lblNewLabel);
		
		JComboBox comboBox = new JComboBox();
		comboBox.addItem("Mois");
		comboBox.addItem("Jours");
		comboBox.setSelectedItem(null);
		comboBox.setBounds(10, 143, 359, 45);
		panel.add(comboBox);
		
		JButton btnNewButton = new JButton("Valider");
		btnNewButton.setBackground(new Color(0, 0, 128));
		btnNewButton.setForeground(new Color(255, 255, 255));
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 19));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				index=comboBox.getSelectedIndex();
				Data data=new Data();
				pointMC=data.pointMortCal(index);
				pointM=data.pointMort(Donnees.seuilDeRentabilite, Donnees.ca, index);
				textField_3.setText(String.valueOf(pointM));
			}
		});
		btnNewButton.setBounds(10, 246, 359, 45);
		panel.add(btnNewButton);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_1.setBounds(406, 48, 626, 359);
		panel_1.setBackground(new Color(248, 248, 255));
		add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("Indice de s\u00E9curit\u00E9");
		lblNewLabel_1.setForeground(new Color(0, 0, 128));
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblNewLabel_1.setBounds(90, 53, 246, 44);
		panel_1.add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("Marge de s\u00E9curit\u00E9");
		lblNewLabel_1_1.setForeground(new Color(0, 0, 128));
		lblNewLabel_1_1.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblNewLabel_1_1.setBounds(90, 112, 246, 44);
		panel_1.add(lblNewLabel_1_1);
		
		JLabel lblNewLabel_1_2 = new JLabel("Levier op\u00E9rationnel");
		lblNewLabel_1_2.setForeground(new Color(0, 0, 128));
		lblNewLabel_1_2.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblNewLabel_1_2.setBounds(90, 167, 246, 44);
		panel_1.add(lblNewLabel_1_2);
		
		textField = new JTextField();
		textField.setBounds(346, 53, 199, 44);
		panel_1.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(346, 113, 199, 44);
		panel_1.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(346, 168, 199, 44);
		panel_1.add(textField_2);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new LineBorder(new Color(0, 0, 128)));
		panel_2.setBounds(69, 242, 476, 2);
		panel_1.add(panel_2);
		
		JLabel lblNewLabel_1_2_1 = new JLabel("Point mort");
		lblNewLabel_1_2_1.setForeground(new Color(0, 0, 128));
		lblNewLabel_1_2_1.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblNewLabel_1_2_1.setBounds(90, 272, 246, 44);
		panel_1.add(lblNewLabel_1_2_1);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(346, 272, 199, 44);
		panel_1.add(textField_3);
		
		JButton btnNewButton_1 = new JButton("Valider");
		btnNewButton_1.setBackground(new Color(0, 0, 128));
		btnNewButton_1.setForeground(new Color(255, 255, 255));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				comboBox.setSelectedItem(null);
				textField.setText("");
				textField_1.setText("");
				textField_2.setText("");
				textField_3.setText("");
				
				CoutSpecifique.panelDonnees.setVisible(false);
				CoutSpecifique.panelIndicateurSec.setVisible(false);
				CoutSpecifique.panelIndicateurRentab.setVisible(false);
				CoutSpecifique.panelCoeffElasticite.setVisible(true);
				
				Util.currentPanel(CoutSpecifique.btnNewButton);
				Util.normalFormat(CoutSpecifique.btnDonnes);
				Util.normalFormat(CoutSpecifique.btnIndicateursDeRentabilits);
				Util.normalFormat(CoutSpecifique.btnIndicateursDeScurit);
			}
		});
		btnNewButton_1.setFont(new Font("Times New Roman", Font.BOLD, 19));
		btnNewButton_1.setBounds(334, 439, 280, 40);
		add(btnNewButton_1);
	}
}
