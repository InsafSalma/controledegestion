import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Authentification extends JFrame {

	private JPanel contentPane;
	private static LogIn panellog;
	private static SignUp panelSignUp; 
	public static Authentification authentificationFrame;
	public static Menu_principal menuFrame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					authentificationFrame = new Authentification();
					authentificationFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Authentification() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 45, 825, 601);
		setTitle("Authentification");
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		panellog=new LogIn();
		panelSignUp=new SignUp();
		
		contentPane.add(panellog);
		panellog.setVisible(true);
		
		contentPane.add(panelSignUp);
		panelSignUp.setVisible(false);
	}
	public static void visibility(int n) {
		
		if(n==0) {
			panellog.setVisible(true);
		}else {
			panelSignUp.setVisible(true);
		}
    }
	public static void  hide(boolean n) {
		if(n) {
			Authentification a=new Authentification();
			a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			a.dispose();
		}
	}
}
