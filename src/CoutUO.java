import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.LineBorder;

import classes.CentreAnalyse;
import classes.Util;
import models.ModelUO;
import javax.swing.JButton;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class CoutUO extends JPanel {
	private JTable table;
	private JScrollPane scrollPane;
	private ModelUO model;

	/**
	 * Create the panel.
	 */
	public CoutUO() {
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setBackground(new Color(248, 248, 255));
		setBounds(0, 45, 1042, 534);
		setLayout(null);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(22, 73, 987, 351);
		add(scrollPane);
		
		JButton btnValider = new JButton("Valider");
		btnValider.setForeground(new Color(255, 255, 255));
		btnValider.setBackground(new Color(0, 0, 128));
		btnValider.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnValider.setBounds(390, 459, 309, 42);
		add(btnValider);
		
		JButton btnNewButton_1 = new JButton("?");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Dans chaque colonne, veuillez:"
						+ "\n - Saisir le montant de la r�partition primaire et les montants respectifs de chaque centre d'analyse auxiliair."
						+ "\n - Saisir la nature de l'UO en lettre puis la nature en chiffre seulement."
						+ "\n - Saisir la quantit� de l'UO."
						+ "\n  !!!Appuyer sur entrer apr�s chaque saisie au tableau!!!", "Informations", 1);
			}
		});
		btnNewButton_1.setForeground(Color.WHITE);
		btnNewButton_1.setFont(new Font("Times New Roman", Font.BOLD, 15));
		btnNewButton_1.setBackground(new Color(0, 0, 128));
		btnNewButton_1.setBounds(22, 26, 59, 36);
		add(btnNewButton_1);
		btnValider.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int i=0;
				for(CentreAnalyse ca : Util.centreAnalysePrincipaux)
				{
					ca.setRepartitionPrimaire(model.getList().get(i).getRepartitionPrimaire());
					ca.setQuantiteUO(model.getList().get(i).getQuantiteUO());
					ca.setNatureUO(model.getList().get(i).getNatureUO());
					ca.setCentresAA(model.getList().get(i).getCentresAA());
					i++;
				}
				setVisible(false);
				CoutComplet.panelAprov.setVisible(true);
				CoutComplet.panelAprov.fillCentresAnalyses();
				
				Util.currentPanel(CoutComplet.btnCotDapprovisionnement);
				Util.normalFormat(CoutComplet.btnCoutRevient);
				Util.normalFormat(CoutComplet.btnCotDeProduction);
				Util.normalFormat(CoutComplet.btnCotDunitDuvre);
				Util.normalFormat(CoutComplet.btnCentresDanalyses);
			}
		});
		
	}
	public void initialiserModel()
	{
		model = new ModelUO(Util.centreAnalyseAuxiliaires,Util.centreAnalysePrincipaux);
		table = new JTable(model);
		CoutComplet.tableStyle(table);
		scrollPane.setViewportView(table);
	}
}
