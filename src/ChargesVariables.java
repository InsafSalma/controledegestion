import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ChargesVariables extends JPanel {

	/**
	 * Create the panel.
	 */
	public ChargesVariables() {
		setBackground(new Color(255, 255, 255));
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 128)));
		panel.setBounds(74, 11, 851, 318);
		panel.setBackground(new Color(248, 248, 255));

		add(panel);
		panel.setLayout(null);
		
		JButton btnNewButton = new JButton("Co\u00FBt variables directs");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				DirectCosting.charges.setVisible(true);
				DirectCosting.btnTitre.setText("Co�t variables directs");
				DirectCosting.btnTitre.setVisible(true);
				Charges.textF.setText("");
				Charges.label.setText("Total : ");
			}
		});
		btnNewButton.setBackground(new Color(0, 0, 205));
		btnNewButton.setForeground(new Color(255, 255, 255));
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnNewButton.setBounds(156, 31, 244, 72);
		panel.add(btnNewButton);
		
		JButton btnCotVariablesIndirects = new JButton("Co\u00FBt variables indirects");
		btnCotVariablesIndirects.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				DirectCosting.charges.setVisible(true);
				DirectCosting.btnTitre.setText("Co�t variables indirects");
				DirectCosting.btnTitre.setVisible(true);
				Charges.textF.setText("");
				Charges.label.setText("Total : ");
			}
		});
		btnCotVariablesIndirects.setForeground(Color.WHITE);
		btnCotVariablesIndirects.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnCotVariablesIndirects.setBackground(new Color(0, 0, 205));
		btnCotVariablesIndirects.setBounds(410, 31, 244, 72);
		panel.add(btnCotVariablesIndirects);
		
		JButton btnTotal = new JButton("Total");
		btnTotal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				DirectCosting.chargesVariablTotal.setVisible(true);
				DirectCosting.btnTitre.setText("Charges variables");
				DirectCosting.btnTitre.setVisible(true);
			}
			
		});
		btnTotal.setForeground(Color.WHITE);
		btnTotal.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnTotal.setBackground(new Color(0, 0, 205));
		btnTotal.setBounds(293, 112, 244, 72);
		panel.add(btnTotal);
		
		JButton btnValider = new JButton("Valider");
		btnValider.setForeground(Color.WHITE);
		btnValider.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnValider.setBackground(new Color(0, 0, 128));
		btnValider.setBounds(185, 262, 215, 45);
		panel.add(btnValider);
		
		JButton btnRetour = new JButton("Retour");
		btnRetour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				DirectCosting.directCostingAcceuil.setVisible(true);
				DirectCosting.btnTitre.setText("");
				DirectCosting.btnTitre.setVisible(false);
			}
		});
		btnRetour.setForeground(Color.WHITE);
		btnRetour.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnRetour.setBackground(new Color(0, 0, 128));
		btnRetour.setBounds(422, 262, 215, 45);
		panel.add(btnRetour);

	}

}
