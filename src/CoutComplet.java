import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import classes.CentreAnalyse;
import classes.CentreAuxiliaire;
import classes.Util;
import models.ModelCentreAnalyse;
import models.ModelCentreAuxiliaire;
import models.ModelUO;

import javax.swing.JMenuBar;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JTable;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;

public class CoutComplet extends JFrame {

	public static JPanel contentPane;
	private Util util;
	public static AjoutProduit panelcoutProd;
	public static CoutProduction panelProNonStock;
	public static CUMPStock panelCUMP;
	public static CoutApprov panelAprov;
	public static CoutUO panelUO;
	public static CentresAnalyses panelCA;
	public static CoutRevient panelCoutRevient;
	public static FIFOStock panelFIFOStock;
	public static JButton btnCoutRevient, btnCotDeProduction, btnCotDapprovisionnement, btnCotDunitDuvre,
			btnCentresDanalyses;

	public CoutComplet() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1056, 616);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		util = new Util();

		panelCoutRevient = new CoutRevient();
		contentPane.add(panelCoutRevient);
		panelCoutRevient.setVisible(false);

		panelcoutProd = new AjoutProduit();
		contentPane.add(panelcoutProd);
		panelcoutProd.setVisible(false);

		panelProNonStock = new CoutProduction();
		contentPane.add(panelProNonStock);
		panelProNonStock.setVisible(false);

		panelCA = new CentresAnalyses();
		contentPane.add(panelCA);

		panelUO = new CoutUO();
		contentPane.add(panelUO);
		panelUO.setVisible(false);

		panelAprov = new CoutApprov();
		contentPane.add(panelAprov);
		panelAprov.setVisible(false);

		panelCUMP = new CUMPStock();
		contentPane.add(panelCUMP);
		panelCUMP.setVisible(false);

		panelFIFOStock = new FIFOStock();
		contentPane.add(panelFIFOStock);
		panelFIFOStock.setVisible(false);

		btnCoutRevient = new JButton("Co\u00FBt de revient");
		btnCoutRevient.setForeground(Color.WHITE);
		btnCoutRevient.setBackground(new Color(0, 0, 128));
		btnCoutRevient.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnCoutRevient.setBounds(910, 0, 132, 43);
		contentPane.add(btnCoutRevient);

		btnCotDeProduction = new JButton("Co\u00FBt de production");
		btnCotDeProduction.setForeground(Color.WHITE);
		btnCotDeProduction.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnCotDeProduction.setBackground(new Color(0, 0, 128));
		btnCotDeProduction.setBounds(757, 0, 159, 43);
		contentPane.add(btnCotDeProduction);

		btnCotDapprovisionnement = new JButton("Co\u00FBt d\u2019approvisionnement");
		btnCotDapprovisionnement.setForeground(Color.WHITE);
		btnCotDapprovisionnement.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnCotDapprovisionnement.setBackground(new Color(0, 0, 128));
		btnCotDapprovisionnement.setBounds(567, 0, 197, 43);
		contentPane.add(btnCotDapprovisionnement);

		btnCotDunitDuvre = new JButton("Co\u00FBt d'unit\u00E9 d'\u0153uvre");
		btnCotDunitDuvre.setForeground(Color.WHITE);
		btnCotDunitDuvre.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnCotDunitDuvre.setBackground(new Color(0, 0, 128));
		btnCotDunitDuvre.setBounds(392, 0, 183, 43);
		contentPane.add(btnCotDunitDuvre);

		btnCentresDanalyses = new JButton("Centres d\u2019analyses");
		btnCentresDanalyses.setForeground(Color.WHITE);
		btnCentresDanalyses.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnCentresDanalyses.setBackground(new Color(0, 0, 128));
		btnCentresDanalyses.setBounds(216, 0, 183, 43);
		contentPane.add(btnCentresDanalyses);
		btnCentresDanalyses.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Util.currentPanel(btnCentresDanalyses);
				Util.normalFormat(btnCotDapprovisionnement);
				Util.normalFormat(btnCotDeProduction);
				Util.normalFormat(btnCotDunitDuvre);
				Util.normalFormat(btnCoutRevient);
			}
		});

		Util.currentPanel(btnCentresDanalyses);

		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent e) {
				try {
					Authentification.menuFrame.setVisible(true);
					e.getWindow().dispose();
				} catch (java.lang.NullPointerException ex) {
					e.getWindow().dispose();
				}
			}
		});
	}

	public static void tableStyle(JTable table) {
		Font fontHeader = new Font("Times New Roman", Font.BOLD, 16);
		Font font = new Font("Times New Roman", Font.PLAIN, 15);
		Color darkBlue = new Color(0, 0, 128);
		table.getTableHeader().setFont(fontHeader);
		table.getTableHeader().setBackground(Color.WHITE);
		table.getTableHeader().setForeground(darkBlue);
		table.setFont(font);
		table.setRowHeight(25);
	}
}
