import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import models.ModelSeuilDeRentab;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class SeuilDeRenSec extends JPanel {
	private JTextField textField;
	private JTable table;
	private ModelSeuilDeRentab modelSeuilDeR;

	/**
	 * Create the panel.
	 */
	public SeuilDeRenSec() {
		setBackground(Color.WHITE);
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 128)));
		panel.setBounds(74, 11, 851, 422);
		panel.setBackground(new Color(248, 248, 255));
		add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Total : ");
		lblNewLabel.setForeground(new Color(0, 0, 128));
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 18));
		lblNewLabel.setBounds(274, 292, 102, 37);
		panel.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(413, 292, 221, 38);
		panel.add(textField);
		
		JButton btnNewButton = new JButton("Calculer");
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnNewButton.setBackground(new Color(0, 0, 128));
		btnNewButton.setBounds(230, 353, 208, 44);
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Retour");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				DirectCosting.directCostingAcceuil.setVisible(true);
				DirectCosting.btnTitre.setText("");
				DirectCosting.btnTitre.setVisible(false);
			}
		});
		btnNewButton_1.setForeground(Color.WHITE);
		btnNewButton_1.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnNewButton_1.setBackground(new Color(0, 0, 128));
		btnNewButton_1.setBounds(460, 353, 208, 44);
		panel.add(btnNewButton_1);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(49, 42, 754, 224);
		panel.add(scrollPane);
		
		modelSeuilDeR=new ModelSeuilDeRentab();
		
		table = new JTable(modelSeuilDeR);
		scrollPane.setViewportView(table);
		
		
	}

}
