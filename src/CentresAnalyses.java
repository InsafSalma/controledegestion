import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import classes.CentreAnalyse;
import classes.CentreAuxiliaire;
import classes.Util;
import models.ModelCentreAnalyse;
import models.ModelCentreAuxiliaire;
import javax.swing.border.CompoundBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import javax.swing.UIManager;

public class CentresAnalyses extends JPanel {

	private JTextField textFieldCAP;
	private JTextField textFieldCAA;
	private JTable centreDanalyse;
	private ModelCentreAnalyse modelCentreAnalyses;
	private ModelCentreAuxiliaire modelCentreAuxiliaire;
	private JTable centreAuxiliaire;

	public CentresAnalyses() {
		setBackground(new Color(248, 248, 255));
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setBounds(0, 45, 1042, 534);
		setLayout(null);

		modelCentreAnalyses = new ModelCentreAnalyse();
		modelCentreAuxiliaire = new ModelCentreAuxiliaire();

		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 255, 255));
		panel.setBorder(new LineBorder(new Color(0, 0, 128)));
		panel.setBounds(40, 62, 468, 393);
		add(panel);
		panel.setLayout(null);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(new LineBorder(new Color(0, 0, 0)));
		scrollPane.setBounds(81, 211, 301, 156);
		panel.add(scrollPane);

		centreDanalyse = new JTable(modelCentreAnalyses);
		CoutComplet.tableStyle(centreDanalyse);
		scrollPane.setViewportView(centreDanalyse);

		JLabel lblNewLabel = new JLabel("Centre d'analyse principal");
		lblNewLabel.setForeground(new Color(0, 0, 128));
		lblNewLabel.setBounds(10, 24, 202, 38);
		panel.add(lblNewLabel);
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 14));

		textFieldCAP = new JTextField();
		textFieldCAP.setBounds(222, 25, 214, 38);
		panel.add(textFieldCAP);
		textFieldCAP.setColumns(10);

		JButton btnAjouterCAP = new JButton("Ajouter");
		btnAjouterCAP.setForeground(new Color(255, 255, 255));
		btnAjouterCAP.setFont(new Font("Times New Roman", Font.BOLD, 20));
		btnAjouterCAP.setBackground(new Color(0, 0, 128));
		btnAjouterCAP.setBounds(10, 93, 426, 39);
		panel.add(btnAjouterCAP);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_2.setBounds(35, 175, 380, 2);
		panel.add(panel_2);

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 128)));
		panel_1.setBackground(Color.WHITE);
		panel_1.setBounds(518, 62, 475, 393);
		add(panel_1);
		panel_1.setLayout(null);

		JLabel lblCentreDanalyseAuxiliaire = new JLabel("Centre d'analyse auxiliaire");
		lblCentreDanalyseAuxiliaire.setForeground(new Color(0, 0, 128));
		lblCentreDanalyseAuxiliaire.setBounds(25, 24, 202, 38);
		panel_1.add(lblCentreDanalyseAuxiliaire);
		lblCentreDanalyseAuxiliaire.setFont(new Font("Times New Roman", Font.BOLD, 14));

		textFieldCAA = new JTextField();
		textFieldCAA.setBounds(237, 25, 214, 38);
		panel_1.add(textFieldCAA);
		textFieldCAA.setColumns(10);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setViewportBorder(new LineBorder(new Color(0, 0, 0)));
		scrollPane_1.setBounds(81, 211, 301, 156);
		panel_1.add(scrollPane_1);

		centreAuxiliaire = new JTable(modelCentreAuxiliaire);
		CoutComplet.tableStyle(centreAuxiliaire);
		scrollPane_1.setViewportView(centreAuxiliaire);

		JButton btnAjouterCAA = new JButton("Ajouter");
		btnAjouterCAA.setBounds(25, 93, 426, 39);
		panel_1.add(btnAjouterCAA);
		btnAjouterCAA.setFont(new Font("Times New Roman", Font.BOLD, 20));
		btnAjouterCAA.setForeground(new Color(255, 255, 255));
		btnAjouterCAA.setBackground(new Color(0, 0, 128));
		
		JPanel panel_2_1 = new JPanel();
		panel_2_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_2_1.setBounds(36, 176, 380, 2);
		panel_1.add(panel_2_1);

		btnAjouterCAA.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String valeur = textFieldCAA.getText();
				CentreAuxiliaire centreAA = new CentreAuxiliaire(valeur);
				modelCentreAuxiliaire.setDonnees(centreAA);
				Util.centreAnalyseAuxiliaires.add(centreAA);
				textFieldCAA.setText("");
			}
		});

		JButton btnNewButton = new JButton("Valider");
		btnNewButton.setForeground(new Color(255, 255, 255));
		btnNewButton.setBackground(new Color(0, 0, 128));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					setVisible(false);
					CoutComplet.panelUO.setVisible(true);
					CoutComplet.panelUO.initialiserModel();
					
					Util.currentPanel(CoutComplet.btnCotDunitDuvre);
					Util.normalFormat(CoutComplet.btnCotDapprovisionnement);
					Util.normalFormat(CoutComplet.btnCotDeProduction);
					Util.normalFormat(CoutComplet.btnCoutRevient);
					Util.normalFormat(CoutComplet.btnCentresDanalyses);
				} catch (Exception e2) {
					JOptionPane.showMessageDialog(null, "Veuillez saisir en moins un centre d'analyse principal! ", "Attention!", 1);
				}
			}
		});
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 20));
		btnNewButton.setBounds(382, 466, 292, 45);
		add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("?");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "- Saisissez le nom du centre d'analyse dans le champ de texte appropri�."
						+ "\n - Appuyez sur le boutton ajouter."
						+ "\n - Appuyez sur le boutton valider pour passer � l'�tape suivante. ", "Informations", 1);
			}
		});
		btnNewButton_1.setForeground(new Color(255, 255, 255));
		btnNewButton_1.setBackground(new Color(0, 0, 128));
		btnNewButton_1.setFont(new Font("Times New Roman", Font.BOLD, 15));
		btnNewButton_1.setBounds(41, 15, 59, 36);
		add(btnNewButton_1);

		btnAjouterCAP.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String valeur = textFieldCAP.getText();
				CentreAnalyse centreA = new CentreAnalyse(valeur);
				modelCentreAnalyses.setDonnees(centreA);
				Util.centreAnalysePrincipaux.add(centreA);
				textFieldCAP.setText("");
			}
		});

	}

}
