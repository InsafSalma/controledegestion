import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;

import models.ModelChargesFixes;

import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Charges extends JPanel {
	public static JTextField textF;
	private JTable table;
	private ModelChargesFixes modelChargesFixes;
	public static JLabel label;

	/**
	 * Create the panel.
	 */
	public Charges() {
		setBackground(new Color(255, 255, 255));
		setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(44, 54, 893, 217);
		add(scrollPane);
		modelChargesFixes=new ModelChargesFixes();
		table = new JTable(modelChargesFixes);
		scrollPane.setViewportView(table);
		
		
		label = new JLabel("Total charges fixes");
		label.setBounds(224, 282, 222, 39);
		label.setForeground(new Color(0, 0, 128));
		label.setFont(new Font("Times New Roman", Font.BOLD, 18));
		add(label);
		
		textF = new JTextField();
		textF.setBounds(456, 282, 241, 39);
		add(textF);
		textF.setColumns(10);
		
		JButton btnNewButton = new JButton("Supprimer ligne");
		btnNewButton.setBounds(181, 361, 162, 39);
		btnNewButton.setBackground(new Color(0, 0, 128));
		btnNewButton.setForeground(new Color(255, 255, 255));
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 18));
		add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Valider");
		btnNewButton_1.setBounds(393, 361, 162, 39);
		btnNewButton_1.setForeground(Color.WHITE);
		btnNewButton_1.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnNewButton_1.setBackground(new Color(0, 0, 128));
		add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Retour");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				DirectCosting.directCostingAcceuil.setVisible(true);
				DirectCosting.btnTitre.setText("");
				DirectCosting.btnTitre.setVisible(false);
			}
		});
		btnNewButton_2.setBounds(608, 361, 162, 39);
		btnNewButton_2.setForeground(Color.WHITE);
		btnNewButton_2.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnNewButton_2.setBackground(new Color(0, 0, 128));
		add(btnNewButton_2);

	}
}
