import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;

public class DirectCosting extends JFrame {

	private JPanel contentPane;
	public static DirectCostingAcceuil directCostingAcceuil;
	public static Charges charges;
	public static ChargesVariables chargesVariables;
	public static ChargesVariablesTotal chargesVariablTotal;
	public static MargeCV margeCV;
	public static Resultat resultat;
	public static SeuilDeRentabilite seuilRen;
	public static TauxMarge tauxM;
	public static SeuilDeRenSec seuilDeRenSec;
	public static LO lo;
	public static JButton btnTitre;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DirectCosting frame = new DirectCosting();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DirectCosting() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1038, 560);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnTitre = new JButton("New button");
		btnTitre.setBackground(new Color(0, 0, 128));
		btnTitre.setForeground(new Color(255, 255, 255));
		btnTitre.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnTitre.setBounds(412, 0, 247, 43);
		contentPane.add(btnTitre);
		btnTitre.setVisible(false);
		
		directCostingAcceuil = new DirectCostingAcceuil();
		directCostingAcceuil.setSize(989, 442);
		directCostingAcceuil.setLocation(20, 54);
		contentPane.add(directCostingAcceuil);

		charges=new Charges();
		charges.setSize(989, 442);
		charges.setLocation(20, 54);
		contentPane.add(charges);
		charges.setVisible(false);
		
		chargesVariables=new ChargesVariables();
		chargesVariables.setSize(989, 442);
		chargesVariables.setLocation(20, 54);
		contentPane.add(chargesVariables);
		chargesVariables.setVisible(false);
		
		chargesVariablTotal=new ChargesVariablesTotal();
		chargesVariablTotal.setSize(989, 442);
		chargesVariablTotal.setLocation(20, 54);
		contentPane.add(chargesVariablTotal);
		chargesVariablTotal.setVisible(false);
		
		margeCV=new MargeCV();
		margeCV.setSize(989, 442);
		margeCV.setLocation(20, 54);
		contentPane.add(margeCV);
		margeCV.setVisible(false);
		
		resultat=new Resultat();
		resultat.setSize(989, 442);
		resultat.setLocation(20, 54);
		contentPane.add(resultat);
		resultat.setVisible(false);
		
		seuilRen=new SeuilDeRentabilite();
		seuilRen.setSize(989, 442);
		seuilRen.setLocation(20, 54);
		contentPane.add(seuilRen);
		seuilRen.setVisible(false);
		
		tauxM=new TauxMarge();
		tauxM.setSize(989, 442);
		tauxM.setLocation(20, 54);
		contentPane.add(tauxM);
		tauxM.setVisible(false);
		
		seuilDeRenSec=new SeuilDeRenSec();
		seuilDeRenSec.setSize(989, 442);
		seuilDeRenSec.setLocation(20, 54);
		contentPane.add(seuilDeRenSec);
		seuilDeRenSec.setVisible(false);
		
		lo=new LO();
		lo.setSize(989, 442);
		lo.setLocation(20, 54);
		contentPane.add(lo);
		lo.setVisible(false);
	}

}
