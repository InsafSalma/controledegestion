import java.sql.*;
import classes.Utilisateur;


public class UtilisateurDAO {
	final static String URL = "jdbc:mysql://localhost:3306/controledegestion";
	final static String LOGIN="root";
	final static String PASS="";
	
	public UtilisateurDAO(){
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e2) {
			System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}
	}
	
	public Utilisateur getUtilisateur(String mail,String mdp) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		Utilisateur retour=null;

		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM utilisateur WHERE pseudo = ? AND motdepasse = ?");
			ps.setString(1,mail);
			ps.setString(2,mdp);
			
			rs=ps.executeQuery();
			if(rs.next())
				retour=new Utilisateur(rs.getString("pseudo"),rs.getString("motdepasse"));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			try {if (rs != null)rs.close();} catch (Exception t) {}
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;
		
	}
	
	public static int ajouter(Utilisateur utilisateur)
	{
		Connection con = null;
		PreparedStatement ps = null;
		int retour=0;
		//connexion � la base de donn�es
		try {

			//tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			//pr�paration de l'instruction SQL, chaque ? repr�sente une valeur communiquer dans l'insertion
			//les getters permettent de r�cup�rer les valeurs des attributs souhait�s
			ps = con.prepareStatement("INSERT INTO utilisateur (nom,prenom,pseudo,motdepasse) VALUES (?, ?,?,?)");
			ps.setString(1,utilisateur.getNom());
			ps.setString(2,utilisateur.getPrenom());
			ps.setString(3,utilisateur.getPseudo());
			ps.setString(4,utilisateur.getMotDePasse());

			retour=ps.executeUpdate();


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du preparedStatement et de la connexion
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}

}
