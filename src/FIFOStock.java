import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import com.toedter.calendar.JDateChooser;

import classes.Entrees;
import classes.MatierePremiere;
import classes.Produit;
import classes.Sorties;
import classes.StockFIFO;
import classes.Util;
import models.ModelFIFO;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FIFOStock extends JPanel {
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	private JComboBox<String> comboBox;
	private ModelFIFO modelFIFO;
	private JTable table;
	private Date date1, date2;
	private double qteStockInitial, qteEntree, qteSortie, prixUEntree, prixInitial;
	private String nomMatiereP;
	private StockFIFO stockFIFO;
	private MatierePremiere matiereP;
	private Entrees entrees;
	private Sorties sorties;
	private ArrayList<Entrees> entreeAr;
	private ArrayList<Sorties> sortieAr;
	private JScrollPane scrollPane;
	private JTextField textField_4;
	private double qteCherche, valorisation, qteReste;
	private boolean afterProduit = false;
	private Produit produit;
	public static JLabel lblNewLabel;

	/**
	 * Create the panel.
	 */
	public FIFOStock() {
		setBackground(new Color(255, 255, 255));
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setBounds(0, 45, 1042, 534);
		setLayout(null);

		entreeAr = new ArrayList<Entrees>();
		sortieAr = new ArrayList<Sorties>();

		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 255, 255));
		panel.setBounds(10, 11, 1022, 324);
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		add(panel);
		panel.setLayout(null);

		lblNewLabel = new JLabel("Mati\u00E8re premi\u00E8re");
		lblNewLabel.setForeground(new Color(0, 0, 128));
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 17));
		lblNewLabel.setBounds(101, 11, 159, 38);
		panel.add(lblNewLabel);

		comboBox = new JComboBox<>();
		comboBox.setBounds(250, 13, 159, 38);
		panel.add(comboBox);

		JLabel lblQuantitDeStock = new JLabel("Quantit\u00E9 de stock initial");
		lblQuantitDeStock.setForeground(new Color(0, 0, 128));
		lblQuantitDeStock.setFont(new Font("Times New Roman", Font.BOLD, 17));
		lblQuantitDeStock.setBounds(441, 11, 194, 38);
		panel.add(lblQuantitDeStock);

		textField = new JTextField();
		textField.setBounds(626, 13, 159, 38);
		panel.add(textField);
		textField.setColumns(10);

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 128)));
		panel_1.setBounds(20, 60, 475, 212);
		panel_1.setBackground(new Color(255, 255, 255));
		panel.add(panel_1);
		panel_1.setLayout(null);

		JLabel lblNewLabel_1 = new JLabel("Entr\u00E9es");
		lblNewLabel_1.setForeground(new Color(0, 0, 128));
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.BOLD, 18));
		lblNewLabel_1.setBounds(181, 11, 86, 30);
		panel_1.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Date");
		lblNewLabel_2.setForeground(new Color(0, 0, 128));
		lblNewLabel_2.setFont(new Font("Times New Roman", Font.BOLD, 18));
		lblNewLabel_2.setBounds(68, 52, 129, 30);
		panel_1.add(lblNewLabel_2);

		JDateChooser dateChooser = new JDateChooser();
		dateChooser.setBounds(232, 52, 129, 30);
		panel_1.add(dateChooser);

		JLabel lblNewLabel_2_1 = new JLabel("Quantit\u00E9");
		lblNewLabel_2_1.setForeground(new Color(0, 0, 128));
		lblNewLabel_2_1.setFont(new Font("Times New Roman", Font.BOLD, 18));
		lblNewLabel_2_1.setBounds(68, 94, 129, 30);
		panel_1.add(lblNewLabel_2_1);

		JLabel lblNewLabel_2_2 = new JLabel("Prix");
		lblNewLabel_2_2.setForeground(new Color(0, 0, 128));
		lblNewLabel_2_2.setFont(new Font("Times New Roman", Font.BOLD, 18));
		lblNewLabel_2_2.setBounds(68, 135, 129, 30);
		panel_1.add(lblNewLabel_2_2);

		comboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				selectedItem(e);
			}
		});

		textField_1 = new JTextField();
		textField_1.setBounds(232, 93, 129, 31);
		panel_1.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(232, 135, 129, 31);
		panel_1.add(textField_2);

		JPanel panel_1_1 = new JPanel();
		panel_1_1.setBorder(new LineBorder(new Color(0, 0, 128)));
		panel_1_1.setBounds(527, 60, 475, 212);
		panel_1_1.setBackground(new Color(255, 255, 255));
		panel.add(panel_1_1);
		panel_1_1.setLayout(null);

		JLabel lblNewLabel_1_1 = new JLabel("Sorties");
		lblNewLabel_1_1.setForeground(new Color(0, 0, 128));
		lblNewLabel_1_1.setFont(new Font("Times New Roman", Font.BOLD, 18));
		lblNewLabel_1_1.setBounds(204, 11, 86, 30);
		panel_1_1.add(lblNewLabel_1_1);

		JLabel lblNewLabel_2_3 = new JLabel("Date");
		lblNewLabel_2_3.setForeground(new Color(0, 0, 128));
		lblNewLabel_2_3.setFont(new Font("Times New Roman", Font.BOLD, 18));
		lblNewLabel_2_3.setBounds(88, 52, 129, 30);
		panel_1_1.add(lblNewLabel_2_3);

		JDateChooser dateChooser_1 = new JDateChooser();
		dateChooser_1.setBounds(252, 52, 129, 30);
		panel_1_1.add(dateChooser_1);

		JLabel lblNewLabel_2_1_1 = new JLabel("Quantit\u00E9");
		lblNewLabel_2_1_1.setForeground(new Color(0, 0, 128));
		lblNewLabel_2_1_1.setFont(new Font("Times New Roman", Font.BOLD, 18));
		lblNewLabel_2_1_1.setBounds(88, 94, 129, 30);
		panel_1_1.add(lblNewLabel_2_1_1);

		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(252, 93, 129, 31);
		panel_1_1.add(textField_3);

		JButton btnNewButton_1 = new JButton("Ajouter les entr\u00E9es");
		btnNewButton_1.setBackground(new Color(0, 0, 128));
		btnNewButton_1.setForeground(new Color(255, 255, 255));
		btnNewButton_1.setFont(new Font("Times New Roman", Font.BOLD, 15));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				date1 = dateChooser.getDate();
				qteEntree = Double.parseDouble(textField_1.getText());
				prixUEntree = Double.parseDouble(textField_2.getText());
				entrees = new Entrees(date1, qteEntree, prixUEntree);
				entreeAr.add(entrees);

				dateChooser.setCalendar(null);
				textField_1.setText("");
				textField_2.setText("");
			}
		});

		JButton btnNewButton_1_1 = new JButton("Ajouter les sorties");
		btnNewButton_1_1.setBackground(new Color(0, 0, 128));
		btnNewButton_1_1.setForeground(new Color(255, 255, 255));
		btnNewButton_1_1.setFont(new Font("Times New Roman", Font.BOLD, 15));
		btnNewButton_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				date2 = dateChooser_1.getDate();
				qteSortie = Double.parseDouble(textField_3.getText());
				sorties = new Sorties(date2, qteSortie);
				sortieAr.add(sorties);

				dateChooser_1.setCalendar(null);
				textField_3.setText("");

			}
		});
		btnNewButton_1_1.setBounds(88, 173, 293, 28);
		panel_1_1.add(btnNewButton_1_1);
		btnNewButton_1.setBounds(68, 173, 293, 28);
		panel_1.add(btnNewButton_1);

		JButton btnNewButton = new JButton("Ajouter");
		btnNewButton.setBackground(new Color(0, 0, 128));
		btnNewButton.setForeground(new Color(255, 255, 255));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				valorisation = 0;
				qteReste = 0;
				qteStockInitial = Double.parseDouble(textField.getText());
				qteReste += qteStockInitial;
				prixInitial = Double.parseDouble(textField_4.getText());
				nomMatiereP = comboBox.getSelectedItem().toString();
				stockFIFO = new StockFIFO(qteStockInitial, prixInitial, entreeAr, sortieAr);

				Collections.sort(entreeAr);
				Collections.sort(sortieAr);
				String msg = stockFIFO.valorisation(entreeAr, sortieAr, qteStockInitial);
				if (msg == "") {
					if(!afterProduit)
					{
						for (MatierePremiere mp : Util.matierePremieres) {
							if (mp.getNom() == matiereP.getNom()) {
								mp.setStockFIFO(stockFIFO);
							}
						}
						modelFIFO.updateDonnees(Util.matierePremieres);
					}else {
						for (Produit p : Util.produits) {
							if (p.getNom() == produit.getNom()) {
								p.setStockFifo(stockFIFO);
							}
						}
						modelFIFO.updateProduits(Util.produits);
					}
					
					entreeAr.clear();
					sortieAr.clear();
					textField.setText("");
					textField_4.setText("");
				} else {
					JOptionPane.showMessageDialog(null, msg + " Veuillez resaisir les informations!", "Attention", 1);
				}

				/*
				 * String nom=matiereP.getNom();
				 * 
				 * matiereP=new
				 * MatierePremiere(nom,stockFIFO.getValorisation(),stockFIFO.getQteReste());
				 * modelFIFO.setDonnees(matiereP);
				 */

			}
		});
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnNewButton.setBounds(398, 283, 217, 35);
		panel.add(btnNewButton);

		JLabel lblNewLabel_3 = new JLabel("Prix");
		lblNewLabel_3.setForeground(new Color(0, 0, 128));
		lblNewLabel_3.setFont(new Font("Times New Roman", Font.BOLD, 17));
		lblNewLabel_3.setBounds(807, 11, 49, 38);
		panel.add(lblNewLabel_3);

		textField_4 = new JTextField();
		textField_4.setBounds(853, 15, 159, 35);
		panel.add(textField_4);
		textField_4.setColumns(10);
		
		JButton btnNewButton_1_2 = new JButton("?");
		btnNewButton_1_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "- Choisissez l'item de la liste d�roulante"
						+ "\n- Saisissez la quantit� initial et son prix unitaire."
						+ "\nPour chaque entr�e:"
						+ "\n- Choisissez la date, saisissez la quantit� et le prix unitaire."
						+ "\n- Appuyer sur le bouton Ajouter les entr�es."
						+ "\nPour chaque sortie:"
						+ "\n- Choisissez la date, saisissez la quantit�"
						+ "\n- Appuyer sur le bouton Ajouter les sorties."
						+ "\n- Appuyer sur le bouton Ajouter."
						+ "\n- Appuyez sur le bouton valider pour passer � l'�tape suivante.", "Informations", 1);
			}
		});
		btnNewButton_1_2.setForeground(Color.WHITE);
		btnNewButton_1_2.setFont(new Font("Times New Roman", Font.BOLD, 15));
		btnNewButton_1_2.setBackground(new Color(0, 0, 128));
		btnNewButton_1_2.setBounds(20, 11, 59, 36);
		panel.add(btnNewButton_1_2);

		scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(new LineBorder(new Color(0, 0, 128)));
		scrollPane.setBounds(81, 346, 876, 143);
		add(scrollPane);

		JButton btnValider = new JButton("Valider");
		btnValider.setForeground(new Color(255, 255, 255));
		btnValider.setBackground(new Color(0, 0, 128));
		btnValider.setFont(new Font("Times New Roman", Font.BOLD, 15));
		btnValider.setBounds(398, 496, 217, 35);
		add(btnValider);
		btnValider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				if(!afterProduit)
				{
					CoutComplet.panelProNonStock.setVisible(true);
					CoutComplet.panelProNonStock.setFifo();
					CoutComplet.panelProNonStock.fillTable();
					CoutComplet.panelProNonStock.fillComboBox();
					CoutComplet.panelProNonStock.isAfterStock(true);
					Util.currentPanel(CoutComplet.btnCotDeProduction);
					Util.normalFormat(CoutComplet.btnCoutRevient);
				}else {
					for(Produit p : Util.produits)
					{
						p.updateCoutProduction();
					}
					CoutComplet.panelCoutRevient.setVisible(true);
					CoutComplet.panelCoutRevient.initialiserModel();
					Util.currentPanel(CoutComplet.btnCoutRevient);
					Util.normalFormat(CoutComplet.btnCotDeProduction);
				}
				Util.normalFormat(CoutComplet.btnCotDapprovisionnement);
				Util.normalFormat(CoutComplet.btnCotDunitDuvre);
				Util.normalFormat(CoutComplet.btnCentresDanalyses);
			}
		});

	}

	public void fillComboBoxAndTable() {
		comboBox.removeAllItems();
		if(!afterProduit)
		{
			for (MatierePremiere mp : Util.matierePremieres) {
				comboBox.addItem(mp.getNom());
			}
			modelFIFO = new ModelFIFO(Util.matierePremieres);
		}else {
			for (Produit p : Util.produits) {
				comboBox.addItem(p.getNom());
			}
			modelFIFO = new ModelFIFO(Util.produits,afterProduit);
		}
		table = new JTable(modelFIFO);
		CoutComplet.tableStyle(table);
		scrollPane.setViewportView(table);
	}

	public void selectedItem(ActionEvent e) {
		if (comboBox.getSelectedItem() != null) {
			if(!afterProduit)
			{
				for (MatierePremiere mp : Util.matierePremieres) {
					if (mp.getNom() == comboBox.getSelectedItem().toString()) {
						matiereP = mp.copy();
					}
				}
			}else {
				for (Produit p : Util.produits) {
					if (p.getNom() == comboBox.getSelectedItem().toString()) {
						produit = p.copy();
					}
				}
			}
		}
	}

	public void isAfterProduit() {
		this.afterProduit = true;
	}
}
