import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import classes.CentreAnalyse;
import classes.MatierePremiere;
import classes.Util;
import models.ModelCoutApprov;
import javax.swing.JComboBox;

public class CoutApprov extends JPanel {
	
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTable table;
	private ModelCoutApprov modelCoutAprov;
	private JComboBox<String> comboBox;
	private CentreAnalyse centreAnalyse;

	/**
	 * Create the panel.
	 */
	public CoutApprov() {
		
			modelCoutAprov=new ModelCoutApprov();
			setLayout(null);
			setBackground(new Color(248, 248, 255));
			setBorder(new LineBorder(new Color(0, 0, 0)));
			setBounds(0, 45, 1042, 534);
			
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setViewportBorder(new LineBorder(new Color(0, 0, 0)));
			scrollPane.setBounds(518, 54, 498, 394);
			add(scrollPane);
			
			table = new JTable(modelCoutAprov);
			CoutComplet.tableStyle(table);
			scrollPane.setViewportView(table);
			
			JPanel panel = new JPanel();
			panel.setBackground(new Color(255, 250, 250));
			
			panel.setBorder(new LineBorder(new Color(0, 0, 0)));
			panel.setBounds(28, 54, 480, 394);
			add(panel);
			panel.setLayout(null);
			
			JLabel lblNewLabel = new JLabel("Mati\u00E8re premi\u00E8re");
			lblNewLabel.setForeground(new Color(0, 0, 128));
			lblNewLabel.setBounds(52, 35, 176, 40);
			panel.add(lblNewLabel);
			lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 15));
			
			JLabel lblQuantitAchet = new JLabel("Quantit\u00E9 achet\u00E9 ");
			lblQuantitAchet.setForeground(new Color(0, 0, 128));
			lblQuantitAchet.setBounds(52, 88, 176, 40);
			panel.add(lblQuantitAchet);
			lblQuantitAchet.setFont(new Font("Times New Roman", Font.BOLD, 15));
			
			JLabel lblPrixUnitaire = new JLabel("Prix unitaire");
			lblPrixUnitaire.setForeground(new Color(0, 0, 128));
			lblPrixUnitaire.setBounds(52, 139, 176, 40);
			panel.add(lblPrixUnitaire);
			lblPrixUnitaire.setFont(new Font("Times New Roman", Font.BOLD, 15));
			
			JLabel lblChargesAdditionnels = new JLabel("Charges additionnels ");
			lblChargesAdditionnels.setForeground(new Color(0, 0, 128));
			lblChargesAdditionnels.setBounds(52, 190, 182, 40);
			panel.add(lblChargesAdditionnels);
			lblChargesAdditionnels.setFont(new Font("Times New Roman", Font.BOLD, 15));
			
			textField = new JTextField();
			textField.setBounds(239, 36, 195, 40);
			panel.add(textField);
			textField.setColumns(10);
			
			textField_1 = new JTextField();
			textField_1.setBounds(239, 88, 195, 40);
			panel.add(textField_1);
			textField_1.setColumns(10);
			
			textField_2 = new JTextField();
			textField_2.setBounds(239, 139, 195, 40);
			panel.add(textField_2);
			textField_2.setColumns(10);
			
			textField_3 = new JTextField();
			textField_3.setBounds(239, 190, 195, 40);
			panel.add(textField_3);
			textField_3.setColumns(10);
			
			JButton btnAjouter = new JButton("Ajouter");
			btnAjouter.setBackground(new Color(0, 0, 128));
			btnAjouter.setForeground(new Color(255, 255, 255));
			btnAjouter.setBounds(52, 317, 382, 40);
			panel.add(btnAjouter);
			btnAjouter.setFont(new Font("Times New Roman", Font.BOLD, 18));
			
			JLabel lblCentreAnalyse = new JLabel("Centre d'analyse");
			lblCentreAnalyse.setForeground(new Color(0, 0, 128));
			lblCentreAnalyse.setFont(new Font("Times New Roman", Font.BOLD, 15));
			lblCentreAnalyse.setBounds(52, 241, 176, 40);
			panel.add(lblCentreAnalyse);
			
			comboBox = new JComboBox<>();
			comboBox.setBounds(239, 241, 195, 40);
			panel.add(comboBox);
			comboBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					for(CentreAnalyse c : Util.centreAnalysePrincipaux)
					{
						if(c.getNom()== comboBox.getSelectedItem().toString())
						{
							centreAnalyse=c;
							break;
						}
					}
				}
			});
			
			JButton btnNewButton = new JButton("Valider");
			btnNewButton.setForeground(new Color(255, 255, 255));
			btnNewButton.setBackground(new Color(0, 0, 128));
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					setVisible(false);
					 CoutComplet.panelcoutProd.setVisible(true);
					 
		            Util.currentPanel(CoutComplet.btnCotDeProduction);
					Util.normalFormat(CoutComplet.btnCotDapprovisionnement);
					Util.normalFormat(CoutComplet.btnCoutRevient);
					Util.normalFormat(CoutComplet.btnCotDunitDuvre);
					Util.normalFormat(CoutComplet.btnCentresDanalyses);
				}
			});
			btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 18));
			btnNewButton.setBounds(403, 465, 232, 42);
			add(btnNewButton);
			
			JButton btnNewButton_1 = new JButton("?");
			btnNewButton_1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					JOptionPane.showMessageDialog(null, "- Saisissez les informations dans leurs champs appropri�s."
							+ "\n- Choisissez un centre d'analyse et appuyer sur le bouton ajouter. "
							+ "\n- Appuyez sur le bouton valider pour passer � l'�tape suivante.", "Informations", 1);
				}
			});
			btnNewButton_1.setForeground(Color.WHITE);
			btnNewButton_1.setFont(new Font("Times New Roman", Font.BOLD, 15));
			btnNewButton_1.setBackground(new Color(0, 0, 128));
			btnNewButton_1.setBounds(28, 11, 59, 36);
			add(btnNewButton_1);
			
			btnAjouter.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					String matierePremiere=textField.getText();
					int quantiteAchete=Integer.parseInt(textField_1.getText());
					double prixUnitaire=Double.parseDouble(textField_2.getText());
					double chargesAdd=Double.parseDouble(textField_3.getText());
					MatierePremiere matiere=new MatierePremiere(matierePremiere, quantiteAchete, prixUnitaire,chargesAdd,centreAnalyse);
					Util.matierePremieres.add(matiere);
					modelCoutAprov.setDonnees(matiere);
					
					textField.setText("");
					textField_1.setText("");
					textField_2.setText("");
					textField_3.setText("");
					
					
				}
			});
		

	}
	public void fillCentresAnalyses()
	{
		comboBox.removeAllItems();
		for (CentreAnalyse ca : Util.centreAnalysePrincipaux) {
			comboBox.addItem(ca.getNom());
		}
	}
}
