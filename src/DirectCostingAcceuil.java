import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Color;
import javax.swing.border.LineBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DirectCostingAcceuil extends JPanel {

	/**
	 * Create the panel.
	 */
	public DirectCostingAcceuil() {
		setLayout(null);
		setBackground(new Color(255, 255, 255));
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 128)));
		panel.setBounds(34, 44, 851, 318);
		panel.setBackground(new Color(248, 248, 255));

		add(panel);
		panel.setLayout(null);
		
		JButton btnNewButton = new JButton("Charges fixes");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				DirectCosting.charges.setVisible(true);
				DirectCosting.btnTitre.setText("Charges fixes");
				DirectCosting.btnTitre.setVisible(true);
			}
		});
		btnNewButton.setBackground(new Color(0, 0, 128));
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnNewButton.setBounds(141, 47, 238, 56);
		panel.add(btnNewButton);
		
		JButton btnChargesVariables = new JButton("Charges variables");
		btnChargesVariables.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				DirectCosting.chargesVariables.setVisible(true);
				DirectCosting.btnTitre.setText("Charges variables");
				DirectCosting.btnTitre.setVisible(true);
			}
		});
		btnChargesVariables.setForeground(Color.WHITE);
		btnChargesVariables.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnChargesVariables.setBackground(new Color(0, 0, 128));
		btnChargesVariables.setBounds(473, 47, 238, 56);
		panel.add(btnChargesVariables);
		
		JButton btnMargeDuCout = new JButton("Marge du co\u00FBt variable");
		btnMargeDuCout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				DirectCosting.margeCV.setVisible(true);
				DirectCosting.btnTitre.setText("Marge du co�t variable");
				DirectCosting.btnTitre.setVisible(true);
				
			}
		});
		btnMargeDuCout.setForeground(Color.WHITE);
		btnMargeDuCout.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnMargeDuCout.setBackground(new Color(0, 0, 128));
		btnMargeDuCout.setBounds(141, 129, 238, 56);
		panel.add(btnMargeDuCout);
		
		JButton btnLevierOprationel = new JButton("Levier op\u00E9rationnel");
		btnLevierOprationel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				DirectCosting.lo.setVisible(true);
				DirectCosting.btnTitre.setText("Levier op�rationnel");
				DirectCosting.btnTitre.setVisible(true);
			}
		});
		btnLevierOprationel.setForeground(Color.WHITE);
		btnLevierOprationel.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnLevierOprationel.setBackground(new Color(0, 0, 128));
		btnLevierOprationel.setBounds(141, 216, 238, 56);
		panel.add(btnLevierOprationel);
		
		JButton btnRsultat = new JButton("R\u00E9sultat");
		btnRsultat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				DirectCosting.resultat.setVisible(true);
				DirectCosting.btnTitre.setText("R�sultat");
				DirectCosting.btnTitre.setVisible(true);
			}
		});
		btnRsultat.setForeground(Color.WHITE);
		btnRsultat.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnRsultat.setBackground(new Color(0, 0, 128));
		btnRsultat.setBounds(473, 129, 238, 56);
		panel.add(btnRsultat);
		
		JButton btnSeuilDeRentabilit = new JButton("Seuil de rentabilit\u00E9");
		btnSeuilDeRentabilit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				DirectCosting.seuilRen.setVisible(true);
				DirectCosting.btnTitre.setText("Seuil de rentabilit� ");
				DirectCosting.btnTitre.setVisible(true);
			}
		});
		btnSeuilDeRentabilit.setForeground(Color.WHITE);
		btnSeuilDeRentabilit.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnSeuilDeRentabilit.setBackground(new Color(0, 0, 128));
		btnSeuilDeRentabilit.setBounds(473, 216, 238, 56);
		panel.add(btnSeuilDeRentabilit);


	}
}
