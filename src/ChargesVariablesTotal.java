import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import models.ModelChargesVariablesTotal;

import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ChargesVariablesTotal extends JPanel {
	private JTextField textField;
	private JTable table;
	private ModelChargesVariablesTotal modelChargesVarTotal;

	/**
	 * Create the panel.
	 */
	public ChargesVariablesTotal() {
		setBackground(new Color(255, 255, 255));
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 128)));
		panel.setBounds(74, 11, 851, 365);
		panel.setBackground(new Color(248, 248, 255));
		add(panel);
		panel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(45, 39, 772, 167);
		panel.add(scrollPane);
		
		modelChargesVarTotal=new ModelChargesVariablesTotal();
		
		table = new JTable(modelChargesVarTotal);
		scrollPane.setViewportView(table);
		
		JLabel lblNewLabel = new JLabel("Total : ");
		lblNewLabel.setForeground(new Color(0, 0, 128));
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 18));
		lblNewLabel.setBounds(244, 217, 102, 37);
		panel.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(383, 217, 221, 38);
		panel.add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("Calculer");
		btnNewButton.setBackground(new Color(0, 0, 128));
		btnNewButton.setForeground(new Color(255, 255, 255));
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnNewButton.setBounds(218, 297, 208, 44);
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Retour");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				DirectCosting.directCostingAcceuil.setVisible(true);
				DirectCosting.btnTitre.setText("");
				DirectCosting.btnTitre.setVisible(false);
			}
		});
		btnNewButton_1.setForeground(Color.WHITE);
		btnNewButton_1.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnNewButton_1.setBackground(new Color(0, 0, 128));
		btnNewButton_1.setBounds(448, 297, 208, 44);
		panel.add(btnNewButton_1);

	}

}
