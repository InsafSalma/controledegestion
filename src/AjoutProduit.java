import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import classes.Produit;
import classes.Util;
import models.ModelCoutApprov;
import models.ModelCoutProduction;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.BevelBorder;

public class AjoutProduit extends JPanel {
	private JTextField textField;
	private JTable table;
	private ModelCoutProduction modelCoutProdution;
	

	/**
	 * Create the panel.
	 */
	public AjoutProduit() {
		setBackground(new Color(255, 255, 255));
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setBounds(0, 45, 1042, 534);
		setLayout(null);
		
		modelCoutProdution=new ModelCoutProduction();
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(255, 250, 250));
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		setBackground(new Color(248, 248, 255));
		panel_1.setBounds(34, 36, 968, 431);
		add(panel_1);
		panel_1.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 128)));
		panel.setBackground(new Color(255, 250, 250));
		panel.setBounds(44, 77, 388, 220);
		panel_1.add(panel);
		panel.setLayout(null);
		
		CoutProduction panelProNonStock=new CoutProduction();
		panelProNonStock.setVisible(false);
		
		textField = new JTextField();
		textField.setBounds(63, 78, 246, 40);
		panel.add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Nom du produit : ");
		lblNewLabel.setForeground(new Color(0, 0, 128));
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 17));
		lblNewLabel.setBounds(63, 27, 246, 40);
		panel.add(lblNewLabel);
		
		JButton btnNewButton = new JButton("Ajouter");
		btnNewButton.setForeground(new Color(255, 255, 255));
		btnNewButton.setBackground(new Color(0, 0, 128));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nomProduit=textField.getText();
				Produit nvProduit=new Produit(nomProduit);
				modelCoutProdution.setDonnees(nvProduit);
				Util.produits.add(nvProduit);
				textField.setText("");
			}
		});
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnNewButton.setBounds(63, 169, 246, 40);
		panel.add(btnNewButton);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(new LineBorder(new Color(0, 0, 0)));
		scrollPane.setBounds(442, 77, 491, 220);
		panel_1.add(scrollPane);
		
		table = new JTable(modelCoutProdution);
		CoutComplet.tableStyle(table);
		scrollPane.setViewportView(table);
		
		JButton btnNewButton_1 = new JButton("Valider");
		btnNewButton_1.setForeground(new Color(255, 255, 255));
		btnNewButton_1.setBackground(new Color(0, 0, 128));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 String[] dialogButton = new String[] {"Oui", "Non"};
				 int dialogResult = JOptionPane.showOptionDialog(null, "Avez-vous du stock ?", "Stock",
				        JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
				        null, dialogButton, dialogButton[0]);
				 
				if(dialogResult == 0) {
					
					String[] options = new String[] {"CUMP", "FIFO"};
				    int response = JOptionPane.showOptionDialog(null, "Vous voulez utiliser quelle m�thode de valorisation des stocks?", "Choix de m�thode",
				        JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
				        null, options, options[0]);
				    
				    if(response==0) {
				    	setVisible(false);
						CoutComplet.panelCUMP.setVisible(true);
						CoutComplet.panelCUMP.fillComboBoxAndTable();
				    }else {
				    	setVisible(false);
						CoutComplet.panelFIFOStock.setVisible(true);
						CoutComplet.panelFIFOStock.fillComboBoxAndTable();
				    }
					
				} else {
					setVisible(false);
		            CoutComplet.panelProNonStock.setVisible(true);
		            CoutComplet.panelProNonStock.noStock = true;
		            CoutComplet.panelProNonStock.fillTable();
		            CoutComplet.panelProNonStock.fillComboBox();;
				}
				  
			}
		});
		
		btnNewButton_1.setFont(new Font("Times New Roman", Font.BOLD, 20));
		btnNewButton_1.setBounds(326, 325, 318, 37);
		panel_1.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("?");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "- Saisissez le nom du produit et appuyez sur le bouton ajouter"
						+ "\n- Appuyez sur le bouton valider pour passer � l'�tape suivante.", "Informations", 1);
			}
		});
		btnNewButton_2.setBackground(new Color(0, 0, 128));
		btnNewButton_2.setForeground(new Color(255, 255, 255));
		btnNewButton_2.setFont(new Font("Times New Roman", Font.BOLD, 16));
		btnNewButton_2.setBounds(44, 29, 49, 37);
		panel_1.add(btnNewButton_2);
	}
}
