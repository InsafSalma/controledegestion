import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import classes.Utilisateur;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import javax.swing.JPasswordField;

public class SignUp extends JPanel {
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JPasswordField passwordField;

	/**
	 * Create the panel.
	 */
	public SignUp() {
		setBackground(Color.WHITE);
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setBounds(0, 45, 988, 527);
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 255, 255));
		panel.setBounds(422, 11, 388, 505);
		add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Nom :");
		lblNewLabel.setBounds(30, 11, 158, 37);
		panel.add(lblNewLabel);
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 17));
		
		textField = new JTextField();
		textField.setBounds(30, 48, 330, 37);
		panel.add(textField);
		textField.setColumns(10);
		
		JLabel lblPrnom = new JLabel("Pr\u00E9nom :");
		lblPrnom.setBounds(30, 114, 158, 37);
		panel.add(lblPrnom);
		lblPrnom.setFont(new Font("Times New Roman", Font.BOLD, 17));
		
		textField_1 = new JTextField();
		textField_1.setBounds(30, 148, 330, 37);
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblNomDutilisateur = new JLabel("Nom d\u2019utilisateur :");
		lblNomDutilisateur.setBounds(30, 221, 158, 37);
		panel.add(lblNomDutilisateur);
		lblNomDutilisateur.setFont(new Font("Times New Roman", Font.BOLD, 17));
		
		textField_2 = new JTextField();
		textField_2.setBounds(30, 257, 330, 37);
		panel.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblMotDePasse = new JLabel("Mot de passe :");
		lblMotDePasse.setBounds(30, 324, 158, 37);
		panel.add(lblMotDePasse);
		lblMotDePasse.setFont(new Font("Times New Roman", Font.BOLD, 17));
		
		JButton btnNewButton = new JButton("S\u2019inscrire");
		btnNewButton.setForeground(new Color(0, 255, 255));
		btnNewButton.setBackground(new Color(0, 0, 0));
		btnNewButton.setBounds(30, 435, 330, 44);
		panel.add(btnNewButton);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int retour;
				Utilisateur utilisateur=new Utilisateur(textField.getText(),textField_1.getText(),textField_2.getText(),passwordField.getText());
				retour = UtilisateurDAO.ajouter(utilisateur);
				
				textField.setText("");
				textField_1.setText("");
				textField_2.setText("");
				passwordField.setText("");
				setVisible(false);
				Authentification.visibility(0);
			}
		});
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 15));
		
		passwordField = new JPasswordField();
		passwordField.setBounds(30, 359, 330, 44);
		panel.add(passwordField);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(0, 0, 0));
		panel_1.setBounds(0, 0, 420, 527);
		add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setBackground(new Color(255, 255, 255));
		lblNewLabel_1.setIcon(new ImageIcon(SignUp.class.getResource("/img/e.jpg")));
		lblNewLabel_1.setBounds(-202, 11, 614, 482);
		panel_1.add(lblNewLabel_1);
	}
}
