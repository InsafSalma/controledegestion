package classes;

import java.util.ArrayList;

public class CentreAnalyse {
	private String nom, natureUO;
	private double quantiteUO, repartitionPrimaire, natureUOChiffre;

	private ArrayList<CentreAuxiliaire> centresAA;

	public CentreAnalyse(String nom, String natureUO, double quantiteUO, double repartitionPrimaire) {
		this.nom = nom;
		this.natureUO = natureUO;
		this.quantiteUO = quantiteUO;
		this.repartitionPrimaire = repartitionPrimaire;
		centresAA = new ArrayList<>();
	}

	public CentreAnalyse(String nom) {
		this.nom = nom;
		centresAA = new ArrayList<>();
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getNatureUO() {
		return natureUO;
	}

	public void setNatureUO(String natureUO) {
		this.natureUO = natureUO;
	}

	public double getQuantiteUO() {
		return quantiteUO;
	}

	public void setQuantiteUO(double quantiteUO) {
		this.quantiteUO = quantiteUO;
	}

	public double getRepartitionPrimaire() {
		return repartitionPrimaire;
	}

	public double getNatureUOChiffre() {
		return natureUOChiffre;
	}

	public void setNatureUOChiffre(double natureUOChiffre) {
		this.natureUOChiffre = natureUOChiffre;
	}

	public void setRepartitionPrimaire(double repartitionPrimaire) {
		this.repartitionPrimaire = repartitionPrimaire;
	}

	public ArrayList<CentreAuxiliaire> getCentresAA() {
		return centresAA;
	}

	public void setCentresAA(ArrayList<CentreAuxiliaire> centresAA) {
		this.centresAA = centresAA;
	}
	public void setCentreAA(CentreAuxiliaire caa) {
		 this.centresAA.add(caa);
	}

	public CentreAuxiliaire getCentreAA(int i) {
		return centresAA.get(i);
	}

	public double calculTotalRepartition() {
		double total = this.repartitionPrimaire;
		for (CentreAuxiliaire caa : centresAA) {
			total += caa.getMontant();
		}
		return total;
	}

	public double calculCoutUO() {
		return this.calculTotalRepartition() / this.quantiteUO;
	}
	public CentreAnalyse copy()
	{
		return new CentreAnalyse(nom, natureUO, quantiteUO, repartitionPrimaire);
	}
}
