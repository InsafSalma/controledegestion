package classes;

public class MatierePremiere {
	private String nom;
	private double prixUnitaire, chargesAdd, montantAppro, valo, stockF;
	private int quantiteAchete;
	private StockCUMP stock = new StockCUMP();
	private CentreAnalyse centreAnalyse;
	private StockFIFO stockFIFO= new StockFIFO();;

	public MatierePremiere(String nom, int quantiteAchete, double prixUnitaire, double chargesAdd,
			CentreAnalyse centreAnalyse) {
		this.nom = nom;
		this.quantiteAchete = quantiteAchete;
		this.prixUnitaire = prixUnitaire;
		this.chargesAdd = chargesAdd;
		this.centreAnalyse = centreAnalyse;
	}

	public MatierePremiere(String nom, int quantiteAchete, double prixUnitaire) {

		this.nom = nom;
		this.quantiteAchete = quantiteAchete;
		this.prixUnitaire = prixUnitaire;

	}

	public MatierePremiere(String nom, double valo, double stockF) {
		super();
		this.nom = nom;
		this.valo = valo;
		this.stockF = stockF;
	}

	public MatierePremiere(String nom, StockFIFO stockFIFO) {
		this.nom = nom;
		this.stockFIFO = stockFIFO;
		this.stockFIFO = new StockFIFO();
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getQuantiteAchete() {
		return quantiteAchete;
	}

	public void setQuantiteAchete(int quantiteAchete) {
		this.quantiteAchete = quantiteAchete;
	}

	public double getPrixUnitaire() {
		return prixUnitaire;
	}

	public void setPrixUnitaire(double prixUnitaire) {
		this.prixUnitaire = prixUnitaire;
	}

	public double getChargesAdd() {
		return chargesAdd;
	}

	public void setChargesAdd(double chargesAdd) {
		this.chargesAdd = chargesAdd;
	}

	public StockCUMP getStock() {
		return stock;
	}

	public void setStock(StockCUMP stock) {
		this.stock = stock.copy();
	}

	public CentreAnalyse getCentreAnalyse() {
		return centreAnalyse;
	}

	public StockFIFO getStockFIFO() {
		return stockFIFO;
	}

	public void setStockFIFO(StockFIFO stockFIFO) {
		this.stockFIFO = stockFIFO.copy();
	}

	public void setCentreAnalyse(CentreAnalyse centreAnalyse) {
		this.centreAnalyse = centreAnalyse;
	}

	public double getValo() {
		return valo;
	}

	public void setValo(double valo) {
		this.valo = valo;
	}

	public double getStockF() {
		return stockF;
	}

	public void setStockF(double stockF) {
		this.stockF = stockF;
	}

	public double getMontantAppro() {
		return montantAppro;
	}

	public void setMontantAppro(double montantAppro) {
		this.montantAppro = montantAppro;
	}

	public double getCout() {
		double montant = this.quantiteAchete * this.prixUnitaire;
		return ((montant / this.centreAnalyse.getNatureUOChiffre()) * this.centreAnalyse.calculCoutUO()) + montant
				+ this.chargesAdd;
	}

	public void calculMontantAfterStock(int quantiteComposantProduit) {
		if(this.stockFIFO.getQteInitial()>0)
		{
			this.montantAppro = this.stockFIFO.getCUMP() * quantiteComposantProduit;
		}else {
			this.montantAppro = this.stock.getCump() * quantiteComposantProduit;
		}
		
	}

	public void calculMontantAppro(int quantiteComposantProduit) {
		this.montantAppro = (getCout() / quantiteAchete) * quantiteComposantProduit;
	}

	public MatierePremiere copy() {
		MatierePremiere mp = new MatierePremiere(nom, quantiteAchete, prixUnitaire, chargesAdd, centreAnalyse);
		mp.setStock(stock);
		mp.setStockFIFO(stockFIFO);
		return mp;
	}
}
