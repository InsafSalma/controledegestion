package classes;

public class CentreAuxiliaire {

	private String nom;
	private double montant;
	public CentreAuxiliaire(String nom, double montant) {
		this.nom = nom;
		this.montant = montant;
	}
	public CentreAuxiliaire(String nom) {
		this.nom = nom;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public double getMontant() {
		return montant;
	}
	public void setMontant(double montant) {
		this.montant = montant;
	}
	public CentreAuxiliaire copy()
	{
		return new CentreAuxiliaire(nom, montant);
	}
	
	
	
}
