package classes;

public class StockCUMP {
	private int quantiteInitial, quantiteFinal, quantiteEntrees, quantiteSorties;
	private double montantInitial, montantEntrees, cump;
	public StockCUMP(int quantiteInitial, int quantiteFinal, int quantiteEntrees, int quantiteSorties,
			double montantInitial, double montantEntrees, double cump) {
		super();
		this.quantiteInitial = quantiteInitial;
		this.quantiteFinal = quantiteFinal;
		this.quantiteEntrees = quantiteEntrees;
		this.quantiteSorties = quantiteSorties;
		this.montantInitial = montantInitial;
		this.montantEntrees = montantEntrees;
		this.cump = cump;
	}
	public StockCUMP() {}
	
	public int getQuantiteInitial() {
		return quantiteInitial;
	}
	public void setQuantiteInitial(int quantiteInitial) {
		this.quantiteInitial = quantiteInitial;
	}
	public int getQuantiteFinal() {
		return quantiteFinal;
	}
	public void setQuantiteFinal(int quantiteFinal) {
		this.quantiteFinal = quantiteFinal;
	}
	public int getQuantiteEntrees() {
		return quantiteEntrees;
	}
	public void setQuantiteEntrees(int quantiteEntrees) {
		this.quantiteEntrees = quantiteEntrees;
	}
	public int getQuantiteSorties() {
		return quantiteSorties;
	}
	public void setQuantiteSorties(int quantiteSorties) {
		this.quantiteSorties = quantiteSorties;
	}
	public double getMontantInitial() {
		return montantInitial;
	}
	public void setMontantInitial(double montantInitial) {
		this.montantInitial = montantInitial;
	}
	public double getMontantEntrees() {
		return montantEntrees;
	}
	public void setMontantEntrees(double montantEntrees) {
		this.montantEntrees = montantEntrees;
	}
	public double getCump() {
		return cump;
	}
	public void setCump(double cump) {
		this.cump = cump;
	}
	public StockCUMP copy()
	{
		return new StockCUMP(quantiteInitial, quantiteFinal, quantiteEntrees, quantiteSorties, montantInitial, montantEntrees, cump);
	}
	public void calculCUMP()
	{
		cump= (montantEntrees+ montantInitial)/(quantiteEntrees+quantiteInitial);
	}
	public double calculMontantFinal()
	{
		return quantiteFinal*cump;
	}
	public double calculMontantSorties()
	{
		return quantiteSorties*cump;
	}
}
