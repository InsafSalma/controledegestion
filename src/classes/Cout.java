package classes;

public class Cout {
	private double coutProduction;
	private double coutHorsProduction;
	private double coutRevient;
	private double quantiteVendu;
	private double prixVente;
	private double resultat;
	
	public Cout() {}
	
	public Cout(double coutProduction, double coutHorsProduction, double coutRevient, double quantiteVendu,
			double prixVente, double resultat) {
		super();
		this.coutProduction = coutProduction;
		this.coutHorsProduction = coutHorsProduction;
		this.coutRevient = coutRevient;
		this.quantiteVendu = quantiteVendu;
		this.prixVente = prixVente;
		this.resultat = resultat;
	}
	
	public double getCoutProduction() {
		return coutProduction;
	}
	public void setCoutProduction(double coutProduction) {
		this.coutProduction = coutProduction;
	}
	public double getCoutHorsProduction() {
		return coutHorsProduction;
	}
	public void setCoutHorsProduction(double coutHorsProduction) {
		this.coutHorsProduction = coutHorsProduction;
	}
	public double getCoutRevient() {
		return coutRevient;
	}
	public void setCoutRevient(double coutRevient) {
		this.coutRevient = coutRevient;
	}
	public double getQuantiteVendu() {
		return quantiteVendu;
	}
	public void setQuantiteVendu(double quantiteVendu) {
		this.quantiteVendu = quantiteVendu;
	}
	public double getPrixVente() {
		return prixVente;
	}
	public void setPrixVente(double prixVente) {
		this.prixVente = prixVente;
	}
	public double getResultat() {
		return resultat;
	}
	public void setResultat(double resultat) {
		this.resultat = resultat;
	}
	public void calculCoutRevient() {
		this.coutRevient= this.coutHorsProduction+this.coutProduction;
	}
	public void calculResultat() {
		this.resultat= (this.quantiteVendu*this.prixVente)-this.coutRevient;
	}
}
