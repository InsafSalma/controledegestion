package classes;

import java.util.Date;

public class Sorties implements Comparable<Sorties> {
	
	private Date dateSortie;
	private double qteSortie;
	public Sorties(Date dateSortie, double qteSortie) {
		
		this.dateSortie = dateSortie;
		this.qteSortie = qteSortie;
	}
	public Date getDateSortie() {
		return dateSortie;
	}
	public void setDateSortie(Date dateSortie) {
		this.dateSortie = dateSortie;
	}
	public double getQteSortie() {
		return qteSortie;
	}
	public void setQteSortie(double qteSortie) {
		this.qteSortie = qteSortie;
	}
	@Override
	public int compareTo(Sorties o) {
		// TODO Auto-generated method stub
		return this.getDateSortie().compareTo(o.getDateSortie());
	}

	
}
