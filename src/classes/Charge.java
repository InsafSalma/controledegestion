package classes;

public class Charge {
	private String nom;
	private double montant;
	private int quantite;
	private CentreAnalyse centreAnalyse;
	
	public Charge(String nom,CentreAnalyse centreAnalyse) {
		this.nom = nom;
		this.centreAnalyse= centreAnalyse;
	}

	public Charge(String nom, double montant, int quantite,CentreAnalyse centreAnalyse) {
		this.nom = nom;
		this.montant = montant;
		this.quantite = quantite;
		this.centreAnalyse= centreAnalyse;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public double getMontant() {
		return montant;
	}

	public void setMontant(double montant) {
		this.montant = montant;
	}

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
	
	public CentreAnalyse getCentreAnalyse() {
		return centreAnalyse;
	}
	
	public void setCentreAnalyse(CentreAnalyse centreAnalyse) {
		this.centreAnalyse = centreAnalyse;
	}

	public void calculeMontant()
	{
		this.montant=this.centreAnalyse.calculCoutUO()*this.quantite;
	}
	public Charge copy()
	{
		return new Charge(nom,montant,quantite ,centreAnalyse);
	}
}
