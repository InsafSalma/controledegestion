package classes;

public class Data {
	private static double chiffreAffaire,chargesVariables,chargesFixes,chargesFixesSpécifiques;
	private static double prixInitial,demandeInitial,prixVariation,demandeVariation;
	public Data() {
		
	}
	public Data(double chiffreAffaire, double chargesVariables, double chargesFixes, double chargesFixesSpécifiques,
			double prixInitial, double demandeInitial, double prixVariation, double demandeVariation) {
		this.chiffreAffaire = chiffreAffaire;
		this.chargesVariables = chargesVariables;
		this.chargesFixes = chargesFixes;
		this.chargesFixesSpécifiques = chargesFixesSpécifiques;
		this.prixInitial = prixInitial;
		this.demandeInitial = demandeInitial;
		this.prixVariation = prixVariation;
		this.demandeVariation = demandeVariation;
	}
	public Data(double chiffreAffaire, double chargesVariables, double chargesFixes, double chargesFixesSpécifiques) {
		this.chiffreAffaire = chiffreAffaire;
		this.chargesVariables = chargesVariables;
		this.chargesFixes = chargesFixes;
		this.chargesFixesSpécifiques = chargesFixesSpécifiques;
	}

	public static double getChiffreAffaire() {
		return chiffreAffaire;
	}

	public static void setChiffreAffaire(double chiffreAffaire) {
		Data.chiffreAffaire = chiffreAffaire;
	}

	public static double getChargesVariables() {
		return chargesVariables;
	}

	public static void setChargesVariables(double chargesVariables) {
		Data.chargesVariables = chargesVariables;
	}

	public static double getChargesFixes() {
		return chargesFixes;
	}

	public static void setChargesFixes(double chargesFixes) {
		Data.chargesFixes = chargesFixes;
	}

	public static double getChargesFixesSpécifiques() {
		return chargesFixesSpécifiques;
	}

	public static void setChargesFixesSpécifiques(double chargesFixesSpécifiques) {
		Data.chargesFixesSpécifiques = chargesFixesSpécifiques;
	}

	public static double getPrixInitial() {
		return prixInitial;
	}

	public static void setPrixInitial(double prixInitial) {
		Data.prixInitial = prixInitial;
	}

	public static double getDemandeInitial() {
		return demandeInitial;
	}

	public static void setDemandeInitial(double demandeInitial) {
		Data.demandeInitial = demandeInitial;
	}

	public static double getPrixVariation() {
		return prixVariation;
	}

	public static void setPrixVariation(double prixVariation) {
		Data.prixVariation = prixVariation;
	}

	public static double getDemandeVariation() {
		return demandeVariation;
	}

	public static void setDemandeVariation(double demandeVariation) {
		Data.demandeVariation = demandeVariation;
	}
	
	public double margeSurCoutVariable(double ca,double cv) {
		return ca-cv;
	}
	
	public double margeSurCoutSpecifique(double mcv, double cfs) {
		return mcv-cfs;
	}
	
	public double resultat(double mcs,double cf) {
		return mcs-cf;
	}
	
	public double seuilDeRentabilite(double mcv,double ca,double cf) {
		double t=mcv/ca;
		return cf/t;
	}
	
	public double tauxMargeSurCoutVariable(double mcv,double ca) {
		return mcv/ca;
	}
	
	public double indiceDeSecurite(double ms,double ca) {
		return ms/ca;
	}
	
	public double margeDeSecurite(double ca,double sr) {
		return ca-sr;
	}
	
	public double levierOp(double mcv, double r) {
		return mcv/r;
	}
	
	public double pointMort(double sr,double ca, int n) {
		if(n==0) {
			return sr/ca*12;
		}else {
			return sr/ca*365;
		}
	}
	
	public String pointMortCal(int n) {
		if(n==0) {
			return "mois";
		}else {
			return "jours";
		}
	}
}
