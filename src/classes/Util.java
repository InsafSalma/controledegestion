package classes;
import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JButton;

public class Util {

	public static ArrayList<CentreAnalyse> centreAnalysePrincipaux;
	public static ArrayList<CentreAuxiliaire> centreAnalyseAuxiliaires;
	public static ArrayList<MatierePremiere> matierePremieres;
	public static ArrayList<Produit> produits;
	public Util() {
		centreAnalysePrincipaux= new ArrayList<>();
		centreAnalyseAuxiliaires = new ArrayList<>();
		matierePremieres = new ArrayList<>();
		produits = new ArrayList<>();
	}
	public static void currentPanel(JButton b) {
		b.setBackground(new Color(100, 149, 237));
		b.setForeground(new Color(255, 255, 255));
	}
	public static void normalFormat(JButton b) {
		b.setBackground(new Color(0, 0, 128));
		b.setForeground(new Color(255, 255, 255));
	}
}
