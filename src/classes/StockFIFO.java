package classes;

import java.util.ArrayList;
import java.util.Date;

public class StockFIFO {
	private double qteInitial, prixInitial;
	private ArrayList<Entrees> entrees;
	private ArrayList<Sorties> sorties;
	private double valorisation = 0, qteReste = 0, qteEntrees = 0;

	public StockFIFO() {
	}

	public StockFIFO(double qteInitial, double prixInitial, ArrayList<Entrees> entrees, ArrayList<Sorties> sorties) {

		this.qteInitial = qteInitial;
		this.prixInitial = prixInitial;
		this.entrees = entrees;
		this.sorties = sorties;
	}

	public StockFIFO(double qteInitial, double prixInitial, ArrayList<Entrees> entrees, ArrayList<Sorties> sorties,
			double valorisation, double qteReste,double qteEntrees) {
		this.qteInitial = qteInitial;
		this.prixInitial = prixInitial;
		this.entrees = entrees;
		this.sorties = sorties;
		this.valorisation = valorisation;
		this.qteReste = qteReste;
		this.qteEntrees=qteEntrees;
	}

	public double getQteInitial() {
		return qteInitial;
	}

	public void setQteInitial(double qteInitial) {
		this.qteInitial = qteInitial;
	}

	public double getPrixInitial() {
		return prixInitial;
	}

	public void setPrixInitial(double prixInitial) {
		this.prixInitial = prixInitial;
	}

	public ArrayList<Entrees> getEntrees() {
		return entrees;
	}

	public void setEntrees(ArrayList<Entrees> entrees) {
		this.entrees = entrees;
	}

	public ArrayList<Sorties> getSorties() {
		return sorties;
	}

	public void setSorties(ArrayList<Sorties> sorties) {
		this.sorties = sorties;
	}

	public String valorisation(ArrayList<Entrees> entreeAr, ArrayList<Sorties> sortieAr, double qteStockInitial) {

		double qteCherche = 0;
		qteReste += qteStockInitial;
		for (Entrees entr : entreeAr) {
			qteReste += entr.getQuantiteEntree();
			qteEntrees += entr.getQuantiteEntree();
		}
		for (Sorties sort : sortieAr) {
			qteCherche += sort.getQteSortie();
		}

		if (qteCherche > qteReste) {
			return "Vous n'avez pas assez de stock pour votre sortie!";
		} else {
			if (qteCherche < qteStockInitial) {
				valorisation += qteCherche * prixInitial;
				qteStockInitial = qteStockInitial - qteCherche;
				qteReste -= qteCherche;
				qteCherche = 0;

			} else {
				valorisation += qteStockInitial * prixInitial;
				qteCherche = qteCherche - qteStockInitial;
				qteReste = qteReste - qteStockInitial;
				qteStockInitial = 0;
				int j = 0;
				while (qteCherche > 0 && j < entreeAr.size()) {
					if (entreeAr.get(j).getQuantiteEntree() > qteCherche) {
						valorisation += qteCherche * entreeAr.get(j).getPrixUEntree();
						qteReste = qteReste - qteCherche;
						qteCherche = 0;
						break;
					} else {
						valorisation += entreeAr.get(j).getQuantiteEntree() * entreeAr.get(j).getPrixUEntree();
						qteCherche = qteCherche - entreeAr.get(j).getQuantiteEntree();
						qteReste = qteReste - entreeAr.get(j).getQuantiteEntree();
						entreeAr.get(j).setQuantiteEntree(0);
						j++;
					}
				}
			}
		}
		return "";
	}

	public double getValorisation() {
		return valorisation;
	}

	public double getQteReste() {
		return qteReste;
	}

	public double getCUMP() {
		double cump = 0;
		cump = this.valorisation / qteEntrees;
		return cump;
	}

	public StockFIFO copy() {
		return new StockFIFO(qteInitial, prixInitial, entrees, sorties, valorisation, qteReste,qteEntrees);
	}

}
