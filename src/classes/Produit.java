package classes;

import java.util.ArrayList;

public class Produit {
	private String nom;
	private Cout cout;
	private ArrayList<MatierePremiere> composants;
	private ArrayList<Charge> charges;
	private StockFIFO stockFifo= new StockFIFO();
	private StockCUMP stockCump= new StockCUMP();

	public Produit(String nom) {
		this.nom = nom;
		this.composants = new ArrayList<>();
		this.charges = new ArrayList<>();
		this.cout = new Cout();
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return nom;
	}

	public StockFIFO getStockFifo() {
		return stockFifo;
	}

	public void setStockFifo(StockFIFO stockFifo) {
		this.stockFifo = stockFifo;
	}

	public StockCUMP getStockCump() {
		return stockCump;
	}

	public void setStockCump(StockCUMP stockCump) {
		this.stockCump = stockCump;
	}

	public ArrayList<MatierePremiere> getComposants() {
		return composants;
	}

	public void setComposant(MatierePremiere matiere) {
		this.composants.add(matiere);
	}

	public void setComposants(ArrayList<MatierePremiere> matieres) {
		this.composants = matieres;
	}

	public ArrayList<Charge> getCharges() {
		return charges;
	}

	public void setCharges(ArrayList<Charge> charges) {
		this.charges = charges;
	}

	public void setCharge(Charge charge) {
		this.charges.add(charge);
	}

	public Cout getCout() {
		return cout;
	}

	public void setCout(Cout cout) {
		this.cout = cout;
	}

	public void calculCoutProduction() {
		double coutP = 0;
		for (MatierePremiere mp : composants) {
			coutP += mp.getMontantAppro();
		}
		for (Charge ch : charges) {
			coutP += ch.getMontant();
		}
		this.cout.setCoutProduction(coutP);
	}
	public void updateCoutProduction() {
		if(this.stockFifo.getQteInitial()>0)
		{
			this.cout.setCoutProduction(this.stockFifo.getValorisation());
		}else {
			this.cout.setCoutProduction(this.stockCump.calculMontantSorties());
		}
		
	}

	public Produit copy() {
		Produit p = new Produit(nom);
		for (Charge ch : charges) {
			p.getCharges().add(ch);
		}
		p.setComposants(composants);
		p.setCout(cout);
		p.setStockCump(stockCump);
		p.setStockFifo(stockFifo);
		return p;
	}
}
