package classes;

import java.util.Date;

public class Entrees implements Comparable<Entrees>{
	private Date dateEntree;
	private double quantiteEntree,prixUEntree;
	
	
	public Entrees(Date date, double quantiteEntree, double prixUEntree) {
		
		this.dateEntree = date;
		this.quantiteEntree = quantiteEntree;
		this.prixUEntree = prixUEntree;
	}
	public Date getDate() {
		return dateEntree;
	}
	public void setDate(Date date) {
		this.dateEntree = date;
	}
	public double getQuantiteEntree() {
		return quantiteEntree;
	}
	public void setQuantiteEntree(double quantiteEntree) {
		this.quantiteEntree = quantiteEntree;
	}
	public double getPrixUEntree() {
		return prixUEntree;
	}
	public void setPrixUEntree(double prixUEntree) {
		this.prixUEntree = prixUEntree;
	}
	@Override
	public int compareTo(Entrees o) {
		// TODO Auto-generated method stub
		return this.getDate().compareTo(o.getDate());
		
	}
	
	

}
