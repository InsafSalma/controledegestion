import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import classes.Utilisateur;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Window;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JFrame;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import javax.swing.JPasswordField;

public class LogIn extends JPanel {
	private JTextField textField;
	private UtilisateurDAO utilisateurDAO;
	private JPasswordField passwordField;

	/**
	 * Create the panel.
	 */
	public LogIn() {
		setBackground(new Color(240, 248, 255));
		
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setBounds(0, 45, 825, 534);
		setLayout(null);
		
		utilisateurDAO=new UtilisateurDAO();
		
		JLabel lblNewLabel = new JLabel("Nom utilisateur:");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lblNewLabel.setBounds(27, 30, 126, 34);
		add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Mot de passe :");
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lblNewLabel_1.setBounds(27, 134, 126, 34);
		add(lblNewLabel_1);
		
		textField = new JTextField();
		textField.setBounds(27, 86, 242, 37);
		add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("Se connecter");
		btnNewButton.setForeground(new Color(240, 248, 255));
		btnNewButton.setBackground(new Color(30, 144, 255));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
						Utilisateur utilis=utilisateurDAO.getUtilisateur(textField.getText(),String.valueOf(passwordField.getText()));
						if(utilis==null)
						{
							JOptionPane.showMessageDialog(null, "Veuillez saisir � nouveau vos informations");
							textField.setText("");
							passwordField.setText("");
						}
						else {
							Authentification.menuFrame=new Menu_principal();
							Authentification.menuFrame.setVisible(true);
							Authentification.authentificationFrame.setVisible(false);
						}
				}
				catch (Exception eo) {
					System.err.println();
					System.err.println("Veuillez controler vos saisies");
				}
			}
		});
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 15));
		btnNewButton.setBounds(27, 274, 242, 39);
		add(btnNewButton);
		
		JLabel lblNewLabel_2 = new JLabel("Vous \u00EAtes pas inscris ?");
		lblNewLabel_2.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblNewLabel_2.setBounds(27, 377, 262, 34);
		add(lblNewLabel_2);
		
		JButton btnNewButton_1 = new JButton("Inscrivez-vous");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				Authentification.visibility(1);
			}
			
		});
		btnNewButton_1.setForeground(new Color(30, 144, 255));
		btnNewButton_1.setBackground(new Color(248, 248, 255));
		btnNewButton_1.setFont(new Font("Times New Roman", Font.BOLD, 15));
		btnNewButton_1.setBounds(27, 411, 242, 39);
		add(btnNewButton_1);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(30, 144, 255));
		panel.setBounds(376, 11, 449, 512);
		add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel_3 = new JLabel("");
		lblNewLabel_3.setIcon(new ImageIcon(LogIn.class.getResource("/img/b.jpg")));
		lblNewLabel_3.setBounds(-41, -11, 490, 512);
		panel.add(lblNewLabel_3);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(27, 179, 242, 39);
		add(passwordField);

	}
}
