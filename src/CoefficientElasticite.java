import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.text.StyleConstants.FontConstants;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.awt.Color;
import java.awt.Desktop;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.awt.event.ActionEvent;

public class CoefficientElasticite extends JPanel {
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private double initialPrix,initialDemande,variationPrix,variationDemande,coeff;
	

	

	/**
	 * Create the panel.
	 */
	public CoefficientElasticite() {
		setBackground(new Color(255, 255, 255));
		setBounds(0, 45, 1042, 534);
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBackground(new Color(248, 248, 255));
		panel.setBounds(38, 44, 980, 335);
		add(panel);
		panel.setLayout(null);
		
		JLabel lblInitial = new JLabel("Initial:");
		lblInitial.setForeground(new Color(0, 0, 139));
		lblInitial.setFont(new Font("Times New Roman", Font.BOLD, 21));
		lblInitial.setBounds(10, 28, 98, 32);
		panel.add(lblInitial);
		
		JLabel lblNewLabel = new JLabel("Prix");
		lblNewLabel.setForeground(new Color(0, 0, 128));
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 17));
		lblNewLabel.setBounds(10, 85, 165, 40);
		panel.add(lblNewLabel);
		
		JLabel lblDemande = new JLabel("Demande");
		lblDemande.setForeground(new Color(0, 0, 128));
		lblDemande.setFont(new Font("Times New Roman", Font.BOLD, 17));
		lblDemande.setBounds(10, 168, 165, 40);
		panel.add(lblDemande);
		
		JLabel lblNewLabel_2 = new JLabel("Prix");
		lblNewLabel_2.setForeground(new Color(0, 0, 128));
		lblNewLabel_2.setFont(new Font("Times New Roman", Font.BOLD, 17));
		lblNewLabel_2.setBounds(495, 85, 165, 40);
		panel.add(lblNewLabel_2);
		
		JLabel lblDemande_1 = new JLabel("Demande");
		lblDemande_1.setForeground(new Color(0, 0, 128));
		lblDemande_1.setFont(new Font("Times New Roman", Font.BOLD, 17));
		lblDemande_1.setBounds(495, 168, 165, 40);
		panel.add(lblDemande_1);
		
		textField = new JTextField();
		textField.setBounds(200, 85, 195, 40);
		panel.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(200, 168, 195, 40);
		panel.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(730, 85, 195, 40);
		panel.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(730, 168, 195, 40);
		panel.add(textField_3);
		
		JLabel lblNewLabel_1 = new JLabel("Variation:");
		lblNewLabel_1.setForeground(new Color(0, 0, 139));
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.BOLD, 21));
		lblNewLabel_1.setBounds(495, 30, 125, 32);
		panel.add(lblNewLabel_1);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 139)));
		panel_1.setBounds(434, 31, 1, 194);
		panel.add(panel_1);
		
		JButton btnNewButton = new JButton("Calculer");
		btnNewButton.setBackground(new Color(0, 0, 128));
		btnNewButton.setForeground(new Color(255, 255, 255));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					initialPrix=Double.parseDouble(textField.getText());
					initialDemande=Double.parseDouble(textField_1.getText());
					variationPrix=Double.parseDouble(textField_2.getText());
					variationDemande=Double.parseDouble(textField_3.getText());
					
					coeff=((initialDemande-variationDemande)/variationDemande)/((initialPrix-variationPrix)/variationPrix);
					textField_4.setText(String.valueOf(coeff));
				} catch (Exception e2) {
					JOptionPane.showMessageDialog(null, "Veuillez saisir toutes les informations! ", "Attention!", 1);
				}
				
			}
		});
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 19));
		btnNewButton.setBounds(319, 259, 282, 40);
		panel.add(btnNewButton);
		
		JLabel lblNewLabel_3 = new JLabel("Coefficient d\u2019\u00E9lasticit\u00E9 Prix/Demande");
		lblNewLabel_3.setForeground(new Color(0, 0, 128));
		lblNewLabel_3.setFont(new Font("Times New Roman", Font.BOLD, 17));
		lblNewLabel_3.setBounds(92, 421, 435, 56);
		add(lblNewLabel_3);
		
		textField_4 = new JTextField();
		textField_4.setBounds(548, 431, 292, 38);
		add(textField_4);
		textField_4.setColumns(10);
		
		JButton btnNewButton_1 = new JButton("Avoir les r\u00E9sultats en PDF");
		btnNewButton_1.setBackground(new Color(0, 0, 128));
		btnNewButton_1.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnNewButton_1.setForeground(Color.WHITE);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Document doc=new Document();
				try {
					PdfWriter.getInstance(doc, new FileOutputStream(".\\Cout specifique.pdf"));
					doc.open();
					Paragraph titre=new Paragraph("Co�t sp�cifique ",FontFactory.getFont("Times-Roman", 20, Font.BOLD));
					titre.setAlignment(Element.ALIGN_CENTER);
					doc.add(titre);
					Paragraph don=new Paragraph("Les donn�es :  \n ",FontFactory.getFont("Times-Roman", 17, Font.BOLD));
					doc.add(don);
					
					Paragraph donInfo=new Paragraph("Chiffre d�affaire :  "+Donnees.ca+
							"\n Charges variables :  "+Donnees.cv + 
							"\nCharges fixes :  "+ Donnees.cf +
							"\nCharges fixes sp�cifique  : "+Donnees.cfs);
					doc.add(donInfo);
					
					Paragraph indiR=new Paragraph("\n Indicateurs de rentabilit�s :  \n ",FontFactory.getFont("Times-Roman", 17, Font.BOLD));
					doc.add(indiR);
					
					Paragraph indiRInfo=new Paragraph("Marge sur co�t variable :  "+Donnees.mcv+
							"\n Marge sur co�t sp�cifique :  "+Donnees.mcs + 
							"\n R�sultat :  "+ Donnees.resultat +
							"\n Seuil de rentabilit�  : "+Donnees.seuilDeRentabilite +
							"\n Taux de marge sur co�t variable  : "+Donnees.tauxMCV
							);
					doc.add(indiRInfo);
					
					Paragraph indiS=new Paragraph("\n Indicateurs de s�curit� :  \n ",FontFactory.getFont("Times-Roman", 17, Font.BOLD));
					doc.add(indiS);
					
					Paragraph indiSInfo=new Paragraph("Indice de s�curit� :  "+IndicateurRentabilite.indiceSec+
							"\n Marge de s�curit� :  "+IndicateurRentabilite.margeSec + 
							"\n Levier op�rationnel :  "+ IndicateurRentabilite.lO +
							"\n Point mort  : "+IndicateurSecurite.pointM +
							". Il est calcul� par : "+ IndicateurSecurite.pointMC
							);
					doc.add(indiSInfo);
					
					Paragraph Coef=new Paragraph("\n Coefficients d��lasticit� :  \n ",FontFactory.getFont("Times-Roman", 17, Font.BOLD));
					doc.add(Coef);
					
					Paragraph init=new Paragraph("Initial :  \n ",FontFactory.getFont("Times-Roman", 15, Font.BOLD));
					doc.add(init);
					
					Paragraph initInfo=new Paragraph("Prix :  "+initialPrix+
							"\n Demande :  "+initialDemande
							);
					doc.add(initInfo);
					
					Paragraph var=new Paragraph("Variation :  \n ",FontFactory.getFont("Times-Roman", 15, Font.BOLD));
					doc.add(var);
					
					Paragraph varInfo=new Paragraph("Prix :  "+variationPrix+
							"\n Demande :  "+variationDemande+
							"\n Coefficient d��lasticit� Prix/Demande "+ coeff
							);
					doc.add(varInfo);
					
					doc.close();
					Desktop.getDesktop().open(new File(".\\Cout specifique.pdf"));
				} catch (FileNotFoundException | DocumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnNewButton_1.setBounds(316, 486, 347, 23);
		add(btnNewButton_1);
		
		
	}
}
