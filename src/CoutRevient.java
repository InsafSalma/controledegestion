import java.awt.Container;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import classes.CentreAnalyse;
import classes.Util;
import models.ModelCoutRevient;
import models.ModelUO;
import java.awt.Color;
import javax.swing.border.LineBorder;

public class CoutRevient extends JPanel {

	private JScrollPane scrollPane;
	private ModelCoutRevient model;
	private JTable table_1;

	/**
	 * Create the panel.
	 */
	public CoutRevient() {
		setBackground(new Color(255, 255, 255));
		setBounds(0, 45, 1042, 534);
		setLayout(null);
		

		scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(new LineBorder(new Color(0, 0, 128)));
		scrollPane.setBounds(22, 73, 987, 351);
		add(scrollPane);
		
		JButton btnNewButton_1 = new JButton("?");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Pour chaque ligne:"
						+ "\n- Saisissez le montant du co�t hors production et appuyer sur entrer."
						+ "\n- Saisissez le prix de vente et la quantit� vendue et appuyer sur entrer.", "Informations", 1);
			}
		});
		btnNewButton_1.setForeground(Color.WHITE);
		btnNewButton_1.setFont(new Font("Times New Roman", Font.BOLD, 15));
		btnNewButton_1.setBackground(new Color(0, 0, 128));
		btnNewButton_1.setBounds(22, 26, 59, 36);
		add(btnNewButton_1);
	}

	public void initialiserModel() {
		model = new ModelCoutRevient(Util.produits);
		table_1 = new JTable(model);
		CoutComplet.tableStyle(table_1);
		scrollPane.setViewportView(table_1);
	}
}
