import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class SeuilDeRentabilite extends JPanel {

	/**
	 * Create the panel.
	 */
	public SeuilDeRentabilite() {
		setBackground(new Color(255, 255, 255));
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(38, 46, 892, 350);
		panel.setBorder(new LineBorder(new Color(0, 0, 128)));
		panel.setBackground(new Color(248, 248, 255));
		add(panel);
		panel.setLayout(null);
		
		JButton btnNewButton = new JButton("Calculer");
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnNewButton.setBackground(new Color(0, 0, 128));
		btnNewButton.setBounds(208, 249, 208, 44);
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Retour");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				DirectCosting.directCostingAcceuil.setVisible(true);
				DirectCosting.btnTitre.setText("");
				DirectCosting.btnTitre.setVisible(false);
			}
		});
		btnNewButton_1.setForeground(Color.WHITE);
		btnNewButton_1.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnNewButton_1.setBackground(new Color(0, 0, 128));
		btnNewButton_1.setBounds(438, 249, 208, 44);
		panel.add(btnNewButton_1);
		
		JButton btnCotVariablesIndirects = new JButton("Calculer le taux de marge");
		btnCotVariablesIndirects.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				DirectCosting.tauxM.setVisible(true);
				DirectCosting.btnTitre.setText("Taux de marge");
				DirectCosting.btnTitre.setVisible(true);
			}
		});
		btnCotVariablesIndirects.setForeground(Color.WHITE);
		btnCotVariablesIndirects.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnCotVariablesIndirects.setBackground(new Color(0, 0, 205));
		btnCotVariablesIndirects.setBounds(101, 66, 334, 72);
		panel.add(btnCotVariablesIndirects);
		
		JButton btnCotVariablesIndirects_1 = new JButton("Calculer le seuil de rentabilit\u00E9");
		btnCotVariablesIndirects_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				DirectCosting.seuilDeRenSec.setVisible(true);
				DirectCosting.btnTitre.setText("Seuil de rentabilité ");
				DirectCosting.btnTitre.setVisible(true);
			}
		});
		btnCotVariablesIndirects_1.setForeground(Color.WHITE);
		btnCotVariablesIndirects_1.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnCotVariablesIndirects_1.setBackground(new Color(0, 0, 205));
		btnCotVariablesIndirects_1.setBounds(440, 66, 334, 72);
		panel.add(btnCotVariablesIndirects_1);

	}

}
