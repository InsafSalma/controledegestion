import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.border.LineBorder;

import classes.Data;
import classes.Util;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Donnees extends JPanel {
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	static double ca,cv,cf,cfs;
	static double mcv,mcs,resultat,seuilDeRentabilite,tauxMCV;

	public Donnees() {
		setBackground(new Color(255, 255, 255));
		setBounds(0, 45, 1042, 534);
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBackground(new Color(248, 248, 255));
		panel.setBounds(88, 55, 861, 352);
		add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Chiffre d\u2019affaire");
		lblNewLabel.setForeground(new Color(0, 0, 128));
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 17));
		lblNewLabel.setBounds(135, 25, 272, 43);
		panel.add(lblNewLabel);
		
		JLabel lblChargesVariables = new JLabel("Charges variables");
		lblChargesVariables.setForeground(new Color(0, 0, 128));
		lblChargesVariables.setFont(new Font("Times New Roman", Font.BOLD, 17));
		lblChargesVariables.setBounds(135, 79, 272, 43);
		panel.add(lblChargesVariables);
		
		JLabel lblChargesFixes = new JLabel("Charges fixes");
		lblChargesFixes.setForeground(new Color(0, 0, 128));
		lblChargesFixes.setFont(new Font("Times New Roman", Font.BOLD, 17));
		lblChargesFixes.setBounds(135, 146, 272, 43);
		panel.add(lblChargesFixes);
		
		JLabel lblChargesFixesSpcifique = new JLabel("Charges fixes sp\u00E9cifique");
		lblChargesFixesSpcifique.setForeground(new Color(0, 0, 128));
		lblChargesFixesSpcifique.setFont(new Font("Times New Roman", Font.BOLD, 17));
		lblChargesFixesSpcifique.setBounds(135, 202, 272, 43);
		panel.add(lblChargesFixesSpcifique);
		
		textField = new JTextField();
		textField.setBounds(439, 25, 307, 43);
		panel.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(439, 79, 307, 43);
		panel.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(439, 146, 307, 43);
		panel.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(439, 202, 307, 43);
		panel.add(textField_3);
		
		JButton btnNewButton = new JButton("Ajouter");
		btnNewButton.setForeground(new Color(255, 255, 255));
		btnNewButton.setBackground(new Color(0, 0, 128));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					ca=Double.parseDouble(textField.getText());
					cv=Double.parseDouble(textField_1.getText());
					cf=Double.parseDouble(textField_2.getText());
					cfs=Double.parseDouble(textField_3.getText());
					Data data=new Data(ca,cv,cf,cfs);
					
					mcv=data.margeSurCoutVariable(ca, cv);
					mcs=data.margeSurCoutSpecifique(mcv, cfs);
					resultat=data.resultat(mcs, cf);
					seuilDeRentabilite=data.seuilDeRentabilite(mcv, ca, cf);
					tauxMCV=data.tauxMargeSurCoutVariable(mcv, ca);
					
					IndicateurRentabilite.textField.setText(String.valueOf(mcv));
					IndicateurRentabilite.textField_1.setText(String.valueOf(mcs));
					IndicateurRentabilite.textField_2.setText(String.valueOf(resultat));
					IndicateurRentabilite.textField_3.setText(String.valueOf(seuilDeRentabilite));
					IndicateurRentabilite.textField_4.setText(String.valueOf(tauxMCV));
					
					textField.setText("");
					textField_1.setText("");
					textField_2.setText("");
					textField_3.setText("");
					CoutSpecifique.panelDonnees.setVisible(false);
					CoutSpecifique.panelIndicateurSec.setVisible(false);
					CoutSpecifique.panelIndicateurRentab.setVisible(true);
					
					Util.currentPanel(CoutSpecifique.btnIndicateursDeRentabilits);
					Util.normalFormat(CoutSpecifique.btnNewButton);
					Util.normalFormat(CoutSpecifique.btnDonnes);
					Util.normalFormat(CoutSpecifique.btnIndicateursDeScurit);
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, "Veuillez saisir toutes les informations! ", "Attention!", 1);
				}
				
			}
		});
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnNewButton.setBounds(298, 278, 287, 43);
		panel.add(btnNewButton);
	}
}
