import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import models.ModelMargeCV;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MargeCV extends JPanel {
	private JTable table;
	private JTextField textField;
	private ModelMargeCV modelMargeCV;

	/**
	 * Create the panel.
	 */
	public MargeCV() {
		setBackground(Color.WHITE);
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 128)));
		panel.setBounds(74, 11, 851, 422);
		panel.setBackground(new Color(248, 248, 255));
		add(panel);
		panel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(31, 54, 786, 225);
		panel.add(scrollPane);
		
		modelMargeCV=new ModelMargeCV();
		
		table = new JTable(modelMargeCV);
		scrollPane.setViewportView(table);
		
		JLabel lblNewLabel = new JLabel("Total : ");
		lblNewLabel.setForeground(new Color(0, 0, 128));
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 18));
		lblNewLabel.setBounds(238, 290, 102, 37);
		panel.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(377, 290, 221, 38);
		panel.add(textField);
		
		JButton btnNewButton = new JButton("Calculer");
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnNewButton.setBackground(new Color(0, 0, 128));
		btnNewButton.setBounds(194, 351, 208, 44);
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Retour");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				DirectCosting.directCostingAcceuil.setVisible(true);
				DirectCosting.btnTitre.setText("");
				DirectCosting.btnTitre.setVisible(false);
			}
		});
		btnNewButton_1.setForeground(Color.WHITE);
		btnNewButton_1.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnNewButton_1.setBackground(new Color(0, 0, 128));
		btnNewButton_1.setBounds(424, 351, 208, 44);
		panel.add(btnNewButton_1);

	}
}
