import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Menu_principal extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu_principal frame = new Menu_principal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Menu_principal() {
		setTitle("Menu principale");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(240, 248, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnCoutComplet = new JButton("Co\u00FBt complet");
		btnCoutComplet.setForeground(new Color(255, 255, 255));
		btnCoutComplet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnCoutComplet.setToolTipText("");
		btnCoutComplet.setBackground(new Color(0, 0, 128));
		btnCoutComplet.setFont(new Font("Times New Roman", Font.BOLD, 16));
		btnCoutComplet.setBounds(10, 50, 205, 68);
		contentPane.add(btnCoutComplet);
		btnCoutComplet.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							CoutComplet frame = new CoutComplet();
							frame.setVisible(true);
							setVisible(false);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});

		JButton btnCotSpcifique = new JButton("Co\u00FBt sp\u00E9cifique");
		btnCotSpcifique.setToolTipText("");
		btnCotSpcifique.setForeground(new Color(255, 255, 255));
		btnCotSpcifique.setFont(new Font("Times New Roman", Font.BOLD, 16));
		btnCotSpcifique.setBackground(new Color(0, 0, 128));
		btnCotSpcifique.setBounds(225, 50, 205, 68);
		contentPane.add(btnCotSpcifique);
		btnCotSpcifique.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							CoutSpecifique frame = new CoutSpecifique();
							frame.setVisible(true);
							setVisible(false);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});

		JButton btnDirectCosting = new JButton("Direct costing");
		btnDirectCosting.setToolTipText("");
		btnDirectCosting.setForeground(new Color(255, 255, 255));
		btnDirectCosting.setFont(new Font("Times New Roman", Font.BOLD, 16));
		btnDirectCosting.setBackground(new Color(0, 0, 128));
		btnDirectCosting.setBounds(116, 152, 205, 68);
		btnDirectCosting.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							DirectCosting frame = new DirectCosting();
							frame.setVisible(true);
							setVisible(false);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		contentPane.add(btnDirectCosting);
	}
}
