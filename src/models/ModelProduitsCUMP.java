package models;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import classes.MatierePremiere;
import classes.Produit;

public class ModelProduitsCUMP extends AbstractTableModel {

	private final String[] titres;
	private final ArrayList<String> titresLignes;
	private ArrayList<MatierePremiere> matieres;
	private ArrayList<Produit> produits;
	private boolean afterProduit;

	public ModelProduitsCUMP(ArrayList<String> lignes, ArrayList<MatierePremiere> matieres) {
		this.matieres=matieres;
		this.titresLignes = lignes;
		this.titres = new String[matieres.size() + 1];
		this.titres[0] = " ";
		int i = 1;
		for (MatierePremiere mp : matieres) {
			titres[i] = mp.getNom();
			i++;
		}
	}
	public ModelProduitsCUMP(ArrayList<String> lignes, ArrayList<Produit> produits,boolean afterProduit) {
		this.afterProduit=afterProduit;
		this.produits=produits;
		this.titresLignes = lignes;
		this.titres = new String[produits.size() + 1];
		this.titres[0] = " ";
		int i = 1;
		for (Produit mp : produits) {
			titres[i] = mp.getNom();
			i++;
		}
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public int getRowCount() {
		return titresLignes.size();
	}

	@Override
	public int getColumnCount() {
		return titres.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		fireTableDataChanged();
		if (columnIndex == 0)
			return titresLignes.get(rowIndex);
		else if(!afterProduit)
		{
			if(rowIndex==0) {
				return this.matieres.get(columnIndex-1).getStock().getMontantInitial();}
			else if(rowIndex==1)
				return this.matieres.get(columnIndex-1).getStock().getMontantEntrees();
			else if(rowIndex==2)
				return this.matieres.get(columnIndex-1).getStock().calculMontantSorties();
			else if(rowIndex==3)
				return this.matieres.get(columnIndex-1).getStock().calculMontantFinal();
		}else
		{
			if(rowIndex==0) {
				return this.produits.get(columnIndex-1).getStockCump().getMontantInitial();}
			else if(rowIndex==1)
				return this.produits.get(columnIndex-1).getStockCump().getMontantEntrees();
			else if(rowIndex==2)
				return this.produits.get(columnIndex-1).getStockCump().calculMontantSorties();
			else if(rowIndex==3)
				return this.produits.get(columnIndex-1).getStockCump().calculMontantFinal();
		}
			return "";
	}

	@Override
	public String getColumnName(int column) {
		return titres[column];
	}

	public void updateMatieres(ArrayList<MatierePremiere> matieres) {
		this.matieres=matieres;
		fireTableDataChanged();
	}
	public void updateProduits(ArrayList<Produit> produits) {
		this.produits=produits;
		fireTableDataChanged();
	}
}
