package models;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import classes.MatierePremiere;


public class ModelCoutApprov extends AbstractTableModel{
	
	private final String[] titres ;
	private final ArrayList<MatierePremiere> donnees;

	public ModelCoutApprov() {
		this.donnees = new ArrayList<>();
		this.titres = new String[] { "Mati�res","Quantit�","Co�t"};
		//this.donnees.add(new CentreAnalyse("dfgh"));
		
	}
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}

	@Override
	public int getRowCount() {
		return donnees.size();
	}

	@Override
	public int getColumnCount() {
		return titres.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		fireTableDataChanged();
		if(columnIndex==0)
			return donnees.get(rowIndex).getNom();
		else if(columnIndex==1)
			return donnees.get(rowIndex).getQuantiteAchete();
		else
			return donnees.get(rowIndex).getCout();
		
		
	}

	@Override
	public String getColumnName(int column) {
		return titres[column];
	}

	public ArrayList<MatierePremiere> getDonnees() {
		return donnees;
	}

	public void setDonnees(MatierePremiere matiere) {
		fireTableDataChanged();
		this.donnees.add(matiere);
	}

	public void setValueAt(Object aValue, int row, int column) {
		donnees.get(row).setNom((String)aValue);
		this.fireTableDataChanged();
	}


	

}
