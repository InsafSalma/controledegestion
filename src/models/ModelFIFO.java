package models;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import classes.MatierePremiere;
import classes.Produit;
import classes.Util;

public class ModelFIFO extends AbstractTableModel {

	private final String[] titres;
	private ArrayList<MatierePremiere> donnees;
	private ArrayList<Produit> produits;
	private boolean afterProduit = false;

	public ModelFIFO(ArrayList<MatierePremiere> matiereP) {
		this.donnees = new ArrayList<>();
		this.titres = new String[] { "Mati�re", "Valorisation des stocks", "Stock final" };
	}

	public ModelFIFO(ArrayList<Produit> produits, boolean afterproduit) {
		this.afterProduit = afterproduit;
		this.produits = produits;
		this.titres = new String[] { "Produits", "Valorisation des stocks", "Stock final" };
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}

	@Override
	public int getRowCount() {
		if(afterProduit)
			return produits.size();
		else
		return donnees.size();
	}

	@Override
	public int getColumnCount() {
		return titres.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		fireTableDataChanged();
		if (!afterProduit) {
			if (columnIndex == 0)
				return donnees.get(rowIndex).getNom();
			else if (columnIndex == 1)
				return donnees.get(rowIndex).getStockFIFO().getValorisation();
			else
				return donnees.get(rowIndex).getStockFIFO().getQteReste();
		} else {
			if (columnIndex == 0)
				return produits.get(rowIndex).getNom();
			else if (columnIndex == 1)
				return produits.get(rowIndex).getStockFifo().getValorisation();
			else
				return produits.get(rowIndex).getStockFifo().getQteReste();
		}

	}

	@Override
	public String getColumnName(int column) {
		return titres[column];
	}

	public ArrayList<MatierePremiere> getDonnees() {
		return donnees;
	}

	public void setDonnees(MatierePremiere matiere) {
		fireTableDataChanged();
		this.donnees.add(matiere);
	}

	public void setValueAt(Object aValue, int row, int column) {

		donnees.get(row).setNom((String) aValue);
		this.fireTableDataChanged();
	}

	public void updateDonnees(ArrayList<MatierePremiere> matieres) {
		this.donnees = matieres;
		fireTableDataChanged();
	}

	public void updateProduits(ArrayList<Produit> produits) {
		this.produits = produits;
		fireTableDataChanged();
	}
}
