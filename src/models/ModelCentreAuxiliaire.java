package models;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;
import classes.CentreAuxiliaire;

public class ModelCentreAuxiliaire extends AbstractTableModel {

	private final String[] titres;
	private final ArrayList<CentreAuxiliaire> donnees;

	public ModelCentreAuxiliaire() {
		this.donnees = new ArrayList<>();
		this.titres = new String[] { "Centres d�analyses auxiliaires"};
		//this.donnees.add(new CentreAnalyse("dfgh"));
		
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}

	@Override
	public int getRowCount() {
		return donnees.size();
	}

	@Override
	public int getColumnCount() {
		return titres.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		fireTableDataChanged();
		return donnees.get(rowIndex).getNom();
		
	}

	@Override
	public String getColumnName(int column) {
		return titres[column];
	}

	public ArrayList<CentreAuxiliaire> getDonnees() {
		return donnees;
	}

	public void setDonnees(CentreAuxiliaire centreAA) {
		fireTableDataChanged();
		this.donnees.add(centreAA);
	}

	public void setValueAt(Object aValue, int row, int column) {
		donnees.get(row).setNom((String)aValue);
		this.fireTableDataChanged();
	}

}
