package models;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import classes.CentreAnalyse;
import classes.StockCUMP;

public class ModelCUMP extends AbstractTableModel {

	private final String[] titres;
	private final ArrayList<String> titresLignes;
	private StockCUMP stock;
	private final int type;
	private boolean afterProduit= false;

	// si stock d'entrees type est egale � 0 sinon est egale � 1
	public ModelCUMP(ArrayList<String> lignes, int type, StockCUMP stock,boolean afterProduit) {
		this.afterProduit=afterProduit;
		this.titresLignes = lignes;
		this.titres = new String[] { " ", "Quantit�", "CUMP", "Montant" };
		this.stock = stock;
		this.type = type;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if(type==0)
		{
			if(!afterProduit)
			{
				return (columnIndex == 1 || columnIndex == 3) && rowIndex == 0;
			}else {
				return ((columnIndex == 1 || columnIndex == 3) && rowIndex == 0) || (columnIndex == 1 && rowIndex == 1);
			}
		}else {
			return columnIndex == 1 && rowIndex != 2;
		}

	}

	@Override
	public int getRowCount() {
		return titresLignes.size();
	}

	@Override
	public int getColumnCount() {
		return titres.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		fireTableDataChanged();
		if (type == 0)
			return valeursTabEntrees(rowIndex, columnIndex);
		else if (type == 1)
			return valeursTabSorties(rowIndex, columnIndex);
		else
			return " ";
	}

	@Override
	public String getColumnName(int column) {
		return titres[column];
	}

	public void setValueAt(Object aValue, int row, int column) {
		this.fireTableDataChanged();
		if (type == 0) {
			if (row == 0) {
				if (column == 1)
					stock.setQuantiteInitial(Integer.parseInt((String) aValue));
				if (column == 3)
					stock.setMontantInitial(Double.parseDouble((String) aValue));
			} else if (row == 1) {
				if (column == 1 && afterProduit)
					stock.setQuantiteEntrees(Integer.parseInt((String) aValue));
			}
		} else if (type == 1) {
			if (row == 0 && column == 1) {
				stock.setQuantiteFinal(Integer.parseInt((String) aValue));
			} else if (row == 1 && column == 1) {
				stock.setQuantiteSorties(Integer.parseInt((String) aValue));
			}
		}

	}

	public Object valeursTabEntrees(int rowIndex, int columnIndex) {
		fireTableDataChanged();
		if (columnIndex == 0)
			return titresLignes.get(rowIndex);
		else if (rowIndex == 0) {
			if (columnIndex == 1)
				return stock.getQuantiteInitial();
			if (columnIndex == 3)
				return stock.getMontantInitial();
		} else if (rowIndex == 1) {
			if (columnIndex == 1)
				return stock.getQuantiteEntrees();
			if (columnIndex == 3)
				return stock.getMontantEntrees();
		} else if (rowIndex == 2) {
			if (columnIndex == 2) {
				stock.calculCUMP();
				return stock.getCump();
			}
			if (columnIndex == 1)
				return stock.getQuantiteEntrees() + stock.getQuantiteInitial();
			if (columnIndex == 3)
				return stock.getMontantEntrees() + stock.getMontantInitial();
		}
		return "";
	}

	public Object valeursTabSorties(int rowIndex, int columnIndex) {
		fireTableDataChanged();
		if (columnIndex == 0)
			return titresLignes.get(rowIndex);
		else if (rowIndex == 0) {
			if (columnIndex == 1)
				return stock.getQuantiteFinal();
			if (columnIndex == 3)
				return stock.calculMontantFinal();
			if (columnIndex == 2) {
				stock.calculCUMP();
				return stock.getCump();
			}
		} else if (rowIndex == 1) {
			if (columnIndex == 1)
				return stock.getQuantiteSorties();
			if (columnIndex == 3)
				return stock.calculMontantSorties();
			if (columnIndex == 2) {
				stock.calculCUMP();
				return stock.getCump();
			}
		} else if (rowIndex == 2) {
			if (columnIndex == 2) {
				stock.calculCUMP();
				return stock.getCump();
			}
			if (columnIndex == 1)
				return stock.getQuantiteFinal() + stock.getQuantiteSorties();
			if (columnIndex == 3)
				return stock.calculMontantFinal() + stock.calculMontantSorties();
		}
		return "";
	}

	public void setStock(StockCUMP stock) {
		this.stock = stock;
	}
	
}
