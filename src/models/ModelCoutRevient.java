package models;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import classes.CentreAnalyse;
import classes.CentreAuxiliaire;
import classes.Cout;
import classes.Produit;

public class ModelCoutRevient extends AbstractTableModel {

	private static String[] titres;
	private final ArrayList<String> titresLignes = new ArrayList<>();
	ArrayList<Produit> produits;

	public ModelCoutRevient(ArrayList<Produit> produits) {
		this.produits = produits;
		for (Produit p : produits) {
			this.titresLignes.add(p.getNom());
		}
		titres = new String[] { "Produits", "Co�t de production", "Co�t hors production", "Co�t de revient",
				"Quantit� vendue", "Prix de vente", "R�sultat" };
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return columnIndex == 2 || columnIndex == 4 || columnIndex == 5;
	}

	@Override
	public int getRowCount() {
		return titresLignes.size();
	}

	@Override
	public int getColumnCount() {
		return titres.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		fireTableDataChanged();
		if (columnIndex == 0)
			return titresLignes.get(rowIndex);
		else if (columnIndex == 1)
			return this.produits.get(rowIndex).getCout().getCoutProduction();
		if (columnIndex == 2)
			return this.produits.get(rowIndex).getCout().getCoutHorsProduction();
		if (columnIndex == 3)
			return this.produits.get(rowIndex).getCout().getCoutRevient();
		if (columnIndex == 4)
			return this.produits.get(rowIndex).getCout().getQuantiteVendu();
		if (columnIndex == 5)
			return this.produits.get(rowIndex).getCout().getPrixVente();
		if (columnIndex == 6)
			return this.produits.get(rowIndex).getCout().getResultat();
		return "";
	}

	@Override
	public String getColumnName(int column) {
		return titres[column];
	}

	public void setValueAt(Object aValue, int row, int column) {
		if (column == 2) {
			this.produits.get(row).getCout().setCoutHorsProduction(Double.parseDouble(aValue.toString()));
			this.produits.get(row).getCout().calculCoutRevient();
		}
		if (column == 4) {
			this.produits.get(row).getCout().setQuantiteVendu(Double.parseDouble(aValue.toString()));
			this.produits.get(row).getCout().calculResultat();
		}
		if (column == 5) {
			this.produits.get(row).getCout().setPrixVente(Double.parseDouble(aValue.toString()));
			this.produits.get(row).getCout().calculResultat();
		}
		this.fireTableDataChanged();
	}

}