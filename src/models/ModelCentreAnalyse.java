package models;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import classes.CentreAnalyse;

public class ModelCentreAnalyse extends AbstractTableModel {

	private final String[] titres;
	private final ArrayList<CentreAnalyse> donnees;

	public ModelCentreAnalyse() {
		this.donnees = new ArrayList<>();
		this.titres = new String[] { "Centres d�analyses principaux"};	
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}

	@Override
	public int getRowCount() {
		return donnees.size();
	}

	@Override
	public int getColumnCount() {
		return titres.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		fireTableDataChanged();
		return donnees.get(rowIndex).getNom();
		
	}

	@Override
	public String getColumnName(int column) {
		return titres[column];
	}

	public ArrayList<CentreAnalyse> getDonnees() {
		return donnees;
	}

	public void setDonnees(CentreAnalyse centreAP) {
		fireTableDataChanged();
		this.donnees.add(centreAP);
	}

	public void setValueAt(Object aValue, int row, int column) {
		donnees.get(row).setNom((String)aValue);
		this.fireTableDataChanged();
	}

}
