package models;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import classes.Produit;

public class ModelResultat extends AbstractTableModel {
	private final String[] titres ;
	private final ArrayList<Produit> donnees;
	
	public ModelResultat() {
		
		this.donnees = new ArrayList<>();
		this.titres = new String[] { "Charges fixes", "Marge sur co�t variable"};
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return donnees.size();
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return titres.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		fireTableDataChanged();
		return donnees.get(rowIndex).getNom();
	}
	 @Override
	public String getColumnName(int column) {
		return titres[column];
	}


	public void setValueAt(Object aValue, int row, int column) {
		
		donnees.get(row).setNom((String)aValue);
		this.fireTableDataChanged();
	}

}
