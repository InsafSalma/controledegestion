package models;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import classes.MatierePremiere;
import classes.Produit;



public class ModelCoutProduction extends AbstractTableModel{

	private final String[] titres ;
	private final ArrayList<Produit> donnees;
	
	public ModelCoutProduction() {
		this.donnees = new ArrayList<>();
		this.titres = new String[] { "Noms des produits"};
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return donnees.size();
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return titres.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		fireTableDataChanged();
		return donnees.get(rowIndex).getNom();
	}
	 @Override
	public String getColumnName(int column) {
		return titres[column];
	}

	public ArrayList<Produit> getDonnees() {
		return donnees;
	}

	public void setDonnees(Produit produit) {
		fireTableDataChanged();
		this.donnees.add(produit);
	}

	public void setValueAt(Object aValue, int row, int column) {
		
		donnees.get(row).setNom((String)aValue);
		this.fireTableDataChanged();
	}

}
