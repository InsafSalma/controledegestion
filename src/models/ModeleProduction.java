package models;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import classes.CentreAnalyse;
import classes.CentreAuxiliaire;
import classes.Charge;
import classes.MatierePremiere;
import classes.Produit;
import classes.Util;

public class ModeleProduction extends AbstractTableModel {

	private static String[] titres;
	private final ArrayList<String> titresLignes = new ArrayList<>();
	private ArrayList<Produit> produits;
	private boolean stock=false;
	private int cpt; // l'indice de la premi�re charge apr�s les matieres premieres

	public ModeleProduction(ArrayList<Produit> produits, ArrayList<MatierePremiere> matieres) {
		this.produits = produits;
		for (MatierePremiere m : matieres) {
			this.titresLignes.add(m.getNom());
		}
		this.titresLignes.add("Co�t de production");
		this.cpt = matieres.size();

		titres = new String[produits.size() + 1];
		titres[0] = "Charges directes et indirectes";
		int i = 1;
		for (Produit p : produits) {
			titres[i] = p.getNom();
			i++;
		}
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public int getRowCount() {
		return titresLignes.size();
	}

	@Override
	public int getColumnCount() {
		return titres.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		fireTableDataChanged();
		if (columnIndex == 0)
			return titresLignes.get(rowIndex);
		else if (rowIndex >= cpt && rowIndex < (titresLignes.size() - 1) && titresLignes.size() > cpt) 
			return this.produits.get(columnIndex - 1).getCharges().get(rowIndex - cpt).getMontant();
		else if (rowIndex >= 0 && rowIndex < cpt) {
			if (this.produits.get(columnIndex - 1).getComposants().size()>rowIndex) 
				return this.produits.get(columnIndex - 1).getComposants().get(rowIndex).getMontantAppro();
		}
		if (rowIndex == this.titresLignes.size() - 1)
			return this.produits.get(columnIndex - 1).getCout().getCoutProduction();

		return "";
	}

	@Override
	public String getColumnName(int column) {
		return titres[column];
	}

	public void refill(Charge charge) {
		fireTableDataChanged();
		for (Produit p : this.produits) {
			   p.setCharge(charge.copy());
		}
		this.titresLignes.add(this.titresLignes.size()-1, charge.getNom());
	}
	public void updateProduits(ArrayList<Produit> produits)
	{
		this.produits= produits;
		fireTableDataChanged();
	}
	public void isAfterStock(boolean stock)
	{
		this.stock=stock;
	}

}
