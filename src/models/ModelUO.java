package models;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import classes.CentreAnalyse;
import classes.CentreAuxiliaire;
import classes.Util;

public class ModelUO extends AbstractTableModel {

	private static String[] titres;
	private final ArrayList<String> titresLignes = new ArrayList<>();
	private final ArrayList<CentreAnalyse> centresAP;
	private int cpt; // nombre de l'indice de la ligne des totaux en fonction des centres Auxililiaires

	public ModelUO(ArrayList<CentreAuxiliaire> centresAnalysesA, ArrayList<CentreAnalyse> centresAnalysesP) {
		this.centresAP = centresAnalysesP;
		this.titresLignes.add("R�partition Primaire");
		for (CentreAuxiliaire caa : centresAnalysesA) {
			this.titresLignes.add(caa.getNom());
		}
		this.titresLignes.add("Total de la r�partition primaire");
		this.titresLignes.add("Nature Unit� d'oeuvre");
		this.titresLignes.add("Nature Unit� d'oeuvre en chiffres");
		this.titresLignes.add("Quantit� Unit� d'oeuvre");
		this.titresLignes.add("Co�t Unit� d'oeuvre");
		this.cpt = centresAnalysesA.size() + 1;

		titres = new String[centresAnalysesP.size() + 1];
		titres[0] = "Centres Analyses Principaux";
		int i = 1;
		for (CentreAnalyse centreAnalyse : centresAnalysesP) {
			for(CentreAuxiliaire caa: centresAnalysesA)
			{
				centreAnalyse.setCentreAA(caa.copy());
			}
			titres[i] = centreAnalyse.getNom();
			i++;
		}
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return rowIndex != this.cpt && rowIndex != this.titresLignes.size() - 1;
	}

	@Override
	public int getRowCount() {
		return titresLignes.size();
	}

	@Override
	public int getColumnCount() {
		return titres.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		fireTableDataChanged();
		if (columnIndex == 0)
			return titresLignes.get(rowIndex);
		else
		    return valeurColonne(rowIndex, columnIndex, columnIndex);
	}

	public Object valeurColonne(int rowIndex, int columnIndex, int n) {
		if (columnIndex == n) {
			if (rowIndex == 0) {
				return this.centresAP.get(n - 1).getRepartitionPrimaire();
			}
			for (int i = 1; i < this.cpt; i++) {
				if (rowIndex == i && columnIndex == n) {
					return this.centresAP.get(n - 1).getCentreAA(i-1).getMontant();
				}
			}
			if(rowIndex == this.cpt)
			{
				return this.centresAP.get(n - 1).calculTotalRepartition();
			}
			if(rowIndex == this.cpt+1)
			{
				return this.centresAP.get(n - 1).getNatureUO();
			}
			if(rowIndex == this.cpt+2)
			{
				return this.centresAP.get(n - 1).getNatureUOChiffre();
			}
			if(rowIndex == this.cpt+3)
			{
				return this.centresAP.get(n - 1).getQuantiteUO();
			}
			if(rowIndex == this.cpt+4)
			{
				return this.centresAP.get(n - 1).calculCoutUO();
			}
		}
		return "";
	}

	@Override
	public String getColumnName(int column) {
		return titres[column];
	}

	public void setValueAt(Object aValue, int row, int column) {
		this.fireTableDataChanged();
		for (int i = 1; i < this.centresAP.size() + 1; i++) {
			setvaleurColonne(row, column, i,aValue);
		}
	}
	
	public void setvaleurColonne(int rowIndex, int columnIndex, int n,Object aValue) {
		if (columnIndex == n) {
			if (rowIndex == 0) {
				this.centresAP.get(n - 1).setRepartitionPrimaire(Double.parseDouble(aValue.toString()));
			}
			for (int i = 1; i < this.cpt; i++) {
				if (rowIndex == i && columnIndex == n) {
					this.centresAP.get(n - 1).getCentreAA(i-1).setMontant(Double.parseDouble(aValue.toString()));
				}
			}
			if(rowIndex == this.cpt+1)
			{
				this.centresAP.get(n - 1).setNatureUO(aValue.toString());
			}
			if(rowIndex == this.cpt+2)
			{
				this.centresAP.get(n - 1).setNatureUOChiffre(Double.parseDouble(aValue.toString()));
			}
			if(rowIndex == this.cpt+3)
			{
				this.centresAP.get(n - 1).setQuantiteUO(Double.parseDouble(aValue.toString()));
			}
		}
	}
	
	public ArrayList<CentreAnalyse> getList()
	{
		return this.centresAP;
	}
}
